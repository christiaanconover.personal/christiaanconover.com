---
title: My Maine Vacation
date: 2010-07-21T01:19:54.000Z
slug: my-maine-vacation

tags:
  - boston island
  - maine
---

This week I'm on my annual Maine vacation. My family owns an island near Boothbay (disclaimer: we're not rich by any means, my grandfather simply had the foresight in the 60s to recognize an amazing deal when he saw one, and we've inherited it) and the family is up here together through Sunday. We try to come at least once every year & this year I've decided to - _shocked face_ - take some photos, videos, etc. to document the trip & the island.

Our property is called [Boston Island](https://www.boston-island.com), and it's [located north of Southport in the Sheepscot River](http://bit.ly/9430uj). It's a 24 acre island, and our family owns 19 of that (though nobody really cares about property lines, and there are only 3 other families that own land). I've been coming up here all my life, and given that I'm an Army brat it's one of the few places I can really call "home". It's where I learned my love of the water & boating, and of seafood (especially lobster!).

So keep an eye out for posts throughout the week of stuff from the island, including videos.
