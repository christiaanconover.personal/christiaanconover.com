---
title: 'AFC Championship 2012: Disappointing Outcome, Amazing Experience'
date: 2013-01-22T22:08:58.000Z
slug: afc-championship-2012-disappointing-outcome-amazing-experience
image:
  src: /files/blog/afc-championship-2012-disappointing-outcome-amazing-experience/gillette-stadium.jpg
  alt: Gillette Stadium
  caption: 'Gillette Stadium, home of the New England Patriots'

tags:
  - 2012 afc championship
  - afc
  - afc championship
  - baltimore ravens
  - football
  - new england patriots
  - nfl
---

We're home from Foxboro after [a full weekend of football and partying](/tags/2012-afc-championship/). It was an excellent weekend, despite a disappointing loss for the Patriots.

We got to Patriot Place around 1:30 on Sunday, and began our tailgating. We didn't have much in the way of tailgate equipment beyond the beer we picked up, but were near some groups that had all sorts of stuff - including a TV running on a car battery that enabled us to watch the NFC Championship game.

![The NFC Championship game on somebody's television in their trunk.](/files/blog/afc-championship-2012-disappointing-outcome-amazing-experience/trunk-tv.jpg)<br>
The NFC Championship game on somebody's television in their trunk

Not only was this my first time ever at Gillette Stadium, but my first-ever NFL game. I'm used to being the odd man out in Maryland being a Patriots fan, so it was pretty incredible to be in an endless see of Pats fans. It was also satisfying to watch our friend the Ravens fan experiencing what we deal with all year. That said, all the fans we encountered were incredibly friendly and welcoming people both to us and to the Ravens fans. The Ravens fans we talked with were equally friendly, to the point that a group of them wanted to take a picture with all of us (Patriots gear and all!) since we'd come up from Baltimore.

Perhaps the most noticeable element of Patriot Place when you first arrive is how HUGE it is. Aside from the stadium itself, there's an entire shopping mall complete with a multiplex surrounding it. Restaurants, bars, souvenir shops, even places like GameStop and Victoria's Secret. The parking lots that ring the complex seem to go on for miles. Being at Patriot Place you get the same feeling you have at an amusement park - it's easy to forget about the world outside the complex.

We headed into the stadium around 5:45, and since our seats were in the upper deck we had to go up quite a ways to get there. Despite being pretty close to the top of the stadium, we still had great visibility of the field.

![The view from our seats at the 2012 AFC Championship in Gillette Stadium.](/files/blog/view-from-the-stands/view.jpg)<br>
The view from our seats at the 2012 AFC Championship in Gillette Stadium

By a few minutes after 6 our area of the stands were pretty much filled. Beers in hand and excitement in the stadium, the kickoff came and it was on. It wasn't all the cold at the start of the game, but the wind picked up and the temperature dropped, and it started to get absolutely freezing. My friend Michael actually [went to the bathroom just to warm up](http://www.michaelspinosa.com/2013/01/21/tough-day-great-experience-afc-championship-day/) at one point. Yet, until the game really started looking grim for us the cold was easy to ignore.

I've heard a lot of people in the past say that Gillette Stadium is quiet because Patriots fans have no passion. I can tell you nothing could be further from the truth. There was tons of noise around me, but since Gillette is such an open venue it's hard for the sound to build. In my section people were cheering, shouting and making plenty of noise.

The mood was very different once it was over and everyone started filing out. The cold had set in, and the disappointment of the loss was palpable. We got back to the car, warmed up, and drove to the IHOP around the corner from our hotel to drown our frustration with bacon and syrup.

Despite a less than desirable outcome, the experience was incredible. The fans are awesome, the venue is impressive, and the game itself was good. My only regret is that I waited until the end of the season to go to a game, since I'll have to wait many months until I'll have another opportunity!

![Our crew in front of Gillette Stadium.](/files/blog/afc-championship-2012-disappointing-outcome-amazing-experience/crew.jpg)<br>
Our crew in front of Gillette Stadium