---
title: 'Unboxing: Kindle Fire'
date: 2011-11-17T20:58:50.000Z
slug: kindlefire-unboxing
image:
  src: /files/blog/kindlefire-unboxing/kindle17.jpg
  alt: Kindle Fire

tags:
  - gadgets
  - kindle fire
  - unboxing
---

My office ordered a Kindle Fire for me to use, and it just arrived! Being an Amazon product, it arrived in what they call "frustration-free packaging" with which I would agree. All I had to do was pull a rip tab to open the box, and there was my Kindle.

![Kindle Fire in the Box](/files/blog/kindlefire-unboxing/box00.jpg)

Below the device itself was the charging cable, and inside the lid was a small card introducing the user to the Kindle Fire. No other material or paperwork.

![Empty Box for the Kindle Fire](/files/blog/kindlefire-unboxing/box01.jpg)

![Kindle Fire and Charger](/files/blog/kindlefire-unboxing/box02.jpg)

The device itself is similar in form factor to the [Blackberry Playbook](http://en.wikipedia.org/wiki/BlackBerry_PlayBook), but [according to a tear down](http://www.theverge.com/2011/11/16/2565949/kindle-fire-root-teardown-source-code) the hardware is actually quite different. The body is shiny black plastic, and the front is just the screen with no buttons or other adornments.

![Kindle Fire Front](/files/blog/kindlefire-unboxing/kindle00.jpg)

The back is black soft touch, with the Kindle logo stamped into it and the Amazon logo printed below.

![Kindle Fire Back](/files/blog/kindlefire-unboxing/kindle01.jpg)

The only buttons or connectors on the device are along the bottom edge: the power button, a Micro USB port, and a 3.5mm headphone jack. That's it.

![Kindle Fire Bottom Edge](/files/blog/kindlefire-unboxing/kindle02.jpg)

Powering it on, the device boots to a screen asking you to choose an avialable wireless network.

![Kindle Fire Initial Boot: Wireless Network](/files/blog/kindlefire-unboxing/kindle03.jpg)

After that, it asks for your time zone.

![Kindle Fire Initial Boot: Time Zone](/files/blog/kindlefire-unboxing/kindle04.jpg)

The device comes pre-programmed for your Amazon account, so you don't have to enter any account information. I actually don't like that as it's a security risk (what if it gets delivered to the wrong address by mistake?), but that's how they do it. It automatically registers itself with your account, and then checks for software updates. There were updates available when I first powered mine on, so it took a little time to download and install those.

![Kindle Fire Update Download](/files/blog/kindlefire-unboxing/kindle05.jpg)

![Kindle Fire Installing Update](/files/blog/kindlefire-unboxing/kindle06.jpg)

You then swipe to unlock (the background of the unlock screen changes periodically to a random pre-designated image).

![Kindle Fire Swipe Unlock](/files/blog/kindlefire-unboxing/kindle07.jpg)

The software then walks you through a few screens of familiarization.

![Kindle Fire Intro Screen 1](/files/blog/kindlefire-unboxing/kindle08.jpg)

![Kindle Fire Intro Screen 2](/files/blog/kindlefire-unboxing/kindle09.jpg)

![Kindle Fire Intro Screen 3](/files/blog/kindlefire-unboxing/kindle10.jpg)

That's it! The Kindle Fire is ready to go. It comes preloaded with a few apps and books, such as Facebook, Amazon, and the Kindle Fire manual.

![Kindle Fire Home Screen](/files/blog/kindlefire-unboxing/kindle11.jpg)

The manual is a Kindle book, and gives you all the information you need about the Kindle Fire.

![Kindle Fire Manual Cover](/files/blog/kindlefire-unboxing/kindle12.jpg)

![Kindle Fire Device Diagram](/files/blog/kindlefire-unboxing/kindle13.jpg)

![Kindle Fire Manual Page](/files/blog/kindlefire-unboxing/kindle14.jpg)

One thing I noticed right off the bat is that Amazon has FINALLY added better font controls for Kindle books.

![Kindle Fire Font Controls](/files/blog/kindlefire-unboxing/kindle15.jpg)

![Kindle Fire Font Faces](/files/blog/kindlefire-unboxing/kindle16.jpg)

I set mine to a slightly larger sans serif font.

That's pretty much as far as I've gotten. After I've used it for a few days I'll write up a review, but so far I'm really enjoying it!
