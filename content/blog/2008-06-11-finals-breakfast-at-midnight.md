---
title: Finals Breakfast at Midnight
date: 2008-06-11T12:21:56.000Z
slug: finals-breakfast-at-midnight

tags:
  - final exams
  - mass maritime
---

Last night, the mess deck staff put on a Finals Breakfast from 2300 until 0100\. I guess they figured that most people would be up that late studying, so they'd give us an opportunity to take a break and grab something to eat. It was pretty good: scrambled eggs, french toast sticks, bacon, sausage, tater tots, and the usual assortment of breakfast sandwiches and omelettes.

There was a very good turnout; just guessing, I'd say that over half the corps of cadets showed up. I happened to be studying for my Coastal Navigation final today (I'm taking a study break this morning to write this), so it was nice to have an excuse to get out of the room and refresh.

Finals will be going on starting today through Tuesday. I only have my Coastal Navigation final this week; all my other finals are on Monday, which is kind of a drag. No big deal though, I'll have all weekend to study so I should be fine.

Countdown until expected departure for the year: 6 days, 3 hours, 39 minutes!
