---
title: More Like a Cruise Ship Than a Working Ship
date: 2008-01-20T19:20:03.000Z
slug: more-like-a-cruise-ship-than-a-working-ship

tags:
  - mass maritime
  - sea term
  - sea term 2008
---

Today we have Sunday at Sea, when we get some well-earned R&R. There are no musters, except for the divisions on watch and maintenance. Watch division continues on its regular schedule, and maintenance division has maintenance in the morning and the rest of the day off. Lunch and dinner are both cook-outs on the helo deck, where you'll find hundreds of cadets covering almost every part of the helo deck and the boat deck above it. Many people are tanning, others are fishing, some are just hanging out and talking. The speakers are set up at the forward end of the helo deck, and Jimmy Buffett is playing continuously. It's a big party, and a nice change from the day-to-day aboard the Enterprise. Tomorrow it's back to work, with Division 1 on maintenance through Wednesday. Tuesday night we're supposed to reach the Atlantic side of the Panama Canal, where we'll wait until our allotted time to go through. I'm really excited about going through it!
