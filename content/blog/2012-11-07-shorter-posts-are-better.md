---
title: Shorter Posts are Better
date: 2012-11-07T16:21:11.000Z
slug: shorter-posts-are-better

tags:
  - blogging
---

I've started posting here with greater frequency as of late, which has made me remember how much I enjoy doing so. I've also started reading more personal blogs of friends and online acquaintances, and I've noticed something that hadn't really dawned on me before: shorter posts are better.

Part of the problem I've had with writing regularly is the amount of time and effort involved in crafting a full blog post. Once I had a topic to write about, it was a couple of hours of planning, writing, editing, re-editing, and finally posting it. About 45 minutes in I'd start to question whether what I was writing was even any good, and sometimes I'd just give up. I'm sure there were a number of posts that I would have actually posted, and may have been halfway decent, if they'd taken me less time to assemble.

Shorter posts get right to the point, and stay there. Rather than add lots of exposition and repetitive examples to illustrate a point, a short post just comes out with it with enough explanation to convey all the details, but without the superfluous language. This dovetails nicely with [a point Om Malik made on his personal blog](http://om.co/2012/09/11/writing-well-for-blogs/). Internet readers have a very limited attention span. Any more than a few paragraphs for a personal blog post, and they're gone. Direct and quick gets the job done.

So in an effort to actually write from time to time, and not bore readers to death, I'm going to try a new approach: writing short posts. I'll write more, you'll read more (maybe), and everything will be right with the world.
