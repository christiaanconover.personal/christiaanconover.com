---
title: 'Starting at MMA: Pre-Orientation Preparation'
date: 2008-03-07T15:12:05.000Z
slug: starting-at-mma-pre-orientation-preparation

tags:
  - mass maritime
  - orientation
  - starting at mma
  - youngie knowledge
---

Orientation is probably the single biggest source of nervousness and apprehension for incoming freshman at Mass Maritime. Last summer when I was talking to some of my soon-to-be shipmates, the vast majority of conversations revolved around Orientation and what people expected it to be like. Many of the rumors my classmates discussed turned out to be just that, but at the time all the information we were talking about amounted to a pretty daunting concept of Orientation. I won't go into details about the actual Orientation process because, let's face it, that would take all the fun & suspense away from any incoming freshmen that might be reading this! I can, however, give you some tips for things to do before arriving that helped me get through Orientation.

**Learn the Youngie Knowledge.** Orientation is much more of a mental test than a physical one. One of the biggest components is the Youngie Knowledge. Luckily, MMA provides a large portion of it online over the summer, to allow freshmen to learn some of it before they arrive. TAKE ADVANTAGE OF THIS. If you come into Orientation knowing this stuff, you'll be head & shoulders above your classmates who don't, and Orientation will be significantly easier. I resisted doing this, but my mom insisted that I do it. She would make me learn and recite something from the Youngie Knowledge each time I wanted to go out with my friends, so that I would be sure to memorize it. While it made me furious at the time, I was grateful she had done it once I got to Orientation. It's not fun, but it's definitely worth the effort up front.

**Get to know your classmates.** As a cadet from outside of New England, I didn't know anybody from back home at Mass Maritime. However, through the Internet I was able to learn names and faces of many of my classmates, and get to know some of them pretty well before ever meeting them in person. I'm not a big Facebook guy, but it turned out to be invaluable in this case. Seeing "familiar" faces when I arrived made me feel much more comfortable about being here, and made it easier to quickly develop new friendships by having already talked to people before arriving. In fact, that was probably the best thing I did to prepare, because even if I had trouble with everything else I had friends that could help me out.

**Get (or stay) in shape.** Orientation does have a significant physical component, so make sure that you're in good shape to handle it. Push-ups, sit-ups and running are the most important things since that's what the PT test consists of, so make sure you can do them well. As long as you're strong in these areas, you should be well prepared for the rest of Orientation.

**Relax.** Orientation is designed to put you under a lot of stress, and see how you perform. If you do the things I've mentioned above, you'll be in a great position coming into it. Once you get here, you'll be immersed into a whole new experience unlike anything you're likely to have done before. While it will be stressful, it's important to stay focused and calm, and not worry too much. Orientation is as good or bad as you make it, so by being prepared and staying as relaxed as possible, you should be able to get through it just fine.
