---
title: One Month To Go – Still A Lot Left To Do
date: 2007-12-12T14:50:03.000Z
slug: one-month-to-go-still-a-lot-left-to-do

tags:
  - mass maritime
  - sea term
  - sea term 2008
  - preparation
---

Today is the one month mark from departing Buzzards Bay for Sea Term 2008\. In fact, as you may or may not have noticed, I've added a countdown on the right side of the page until we are scheduled to cast off from the pier, and head out to sea. It's incredible to think that in a mere 30 days, we'll be standing on the decks of the T.S. Enterprise, leaving for a once in a lifetime journey through the Caribbean and the Pacific!

However, there's still a lot left to do before we can wave goodbye to our families. Academically, we are all getting ready for final exams next week, which for some may be the deciding factor as to whether or not they go on cruise. Vessel Familiarization has provided its share of challenges this week, with the Survival Exam in the classroom portion, and the knot tying and splicing qualifications in the lab portion. Last week we took the written part of the Vessel Fam lab examination, testing us on both our knowledge of the lifeboat commands, as well as basic knot tying and shipboard terminology. This week, you can find many cadets practicing knots and splicing during study hours, and helping shipmates who don't know their knots that well.

Regimentally, we have to get ready to move out of the dorms and on to the ship for two months. The companies have to be cleaned to an Admirals' inspection standard, and our rooms have to be completely empty by the time we leave next Friday. We've been encouraged to start taking things home so that we have less to worry about next week, which also benefits us now since it's less to worry about for inspections!

The next week and a half are going to be incredibly busy - and somewhat stressful - for cadets. I am fortunate enough to only have a couple of exams next week, but I know that some of my shipmates are not so lucky. Good luck to all my shipmates next week - Hopefully we'll all be on cruise in a month!
