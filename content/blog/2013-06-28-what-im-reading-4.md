---
title: "What I'm Reading"
date: 2013-06-28T14:20:35.000Z
slug: what-im-reading-4
image:
  src: /files/blog/what-im-reading-4/eating.jpg
  alt: Plate of food

tags:
  - biotech
  - bitcoin
  - eating
  - nsa
  - "what i'm reading"
---

[**The electrified brain: the power and promise of neural implants**](http://www.theverge.com/2013/6/27/4431274/the-electrified-brain-the-power-and-promise-of-neural-implants)<br>
We really are getting to the point of building bionic humans. Brain implant technology is the most fascinating area of development in that space.

[**The Criminal NSA**](http://www.nytimes.com/2013/06/28/opinion/the-criminal-nsa.html)<br>
Nearly all the coverage of PRISM and NSA's surveillance is around Edward Snowden's illegal actions. Let's take a look at the illegalities of NSA's oeprations.

[**Put a Fork in It**](http://www.slate.com/articles/life/culturebox/2013/06/fork_and_knife_use_americans_need_to_stop_cutting_and_switching.single.html)<br>
I don't do this so I've never thought about it, but apparently a lot of Americans have a very inefficient and awkward way of using their fork & knife.

[**Bitcoin: Are we looking at a revolution?**](http://venturebeat.com/2013/06/27/bitcoin-are-we-looking-at-a-revolution/)<br>
Bitcoin is far more than just a hobbyist way for anonymous transactions. It's a whole new infrastructure for digital payments that cuts out the middlemen.
