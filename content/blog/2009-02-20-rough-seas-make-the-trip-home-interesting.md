---
title: Rough Seas Make the Trip Home Interesting
date: 2009-02-20T17:59:18.000Z
slug: rough-seas-make-the-trip-home-interesting

tags:
  - mass maritime
  - sea term
  - sea term 2009
---

For the past couple of days we've been in rough weather which has kept the ship rolling and pitching almost constantly since Wednesday. This is the first legitimately rough weather we've had on cruise, so it's been exciting.

The most interesting and hilarious part of rough seas aboard the Kennedy is being in the mess deck. when we take big rolls, people and food go flying in all directions - sliding across tables, crashing into each other, falling over with full trays of food in hand, etc. Last night there was a freshman who lost his balance right by the end of my table, fell over the end of the table and started to slide down on his stomach, dumping his cup of orange soda all over the table!

While I usually find rolling to be helpful when it comes to sleeping, this is too much rolling to facilitate good sleep, and in fact nobody is getting much sleep at all. Most people are woken up throughout the night from rolling into the wall in their rack, or pushing up against the safety strap on the outside.

I'm enjoying it, despite some of the inconveniences it causes. I find that one of the strangest feelings associated with this weather is having to make an effort to climb down a flight of stairs.

Everyone is looking past the rough weather at this point, and focusing on finishing exams. Today is exam day, the final hurdle in completing cruise. I have the practical portion of my final at 1400, about an hour from now. Once I'm finished with that, I'm 100% done with all my cruise requirements. I'm pretty excited for that, to say the least.

I'm going to sign off for now and go get ready for my practical final. I have a few guest posters lined up, but they seem to be taking their time writing their posts, so hopefully I'll get those tonight or tomorrow.
