---
title: The History of the Greenbury Point Radio Towers
date: 2014-01-17T18:46:57.000Z
slug: the-history-of-the-greenbury-point-radio-towers

tags:
  - annapolis
  - greenbury point
  - history
  - navy
  - nss annapolis
---

Anyone who's spent time in Annapolis is likely familiar with the [radio towers on Greenbury Point](https://en.wikipedia.org/wiki/NSS_Annapolis). The three remaining towers, at 800' high each, are visible for miles and are a major landmark for residents and travelers. However, the towers as they stand today are just the remnants of what was once a massive complex of naval communications.

A YouTube channel called [A History of the Navy in 100 Objects](https://www.youtube.com/user/AHistoryoftheNavy) has produced a video that covers the history of the facility that stood for over 80 years. It's got great footage of the tower array over the years, and interviews with people familiar with the facility.

{{< youtube jWuJ6DB9drw >}}