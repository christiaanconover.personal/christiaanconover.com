---
title: Arrival in Puerto Rico
date: 2009-01-31T04:56:07.000Z
slug: arrival-in-puerto-rico

tags:
  - mass maritime
  - puerto rico
  - san juan
  - sea term
  - sea term 2009
---

We arrived in San Juan, Puerto Rico around 0830 this morning. After a couple of hiccups with the electronic check on/off system, everyone granted liberty was off the ship and out into port. The weather was perfect for a warm welcome into Puerto Rico.

We got lucky and had a last-minute berth re-assignment last night which put us at Pier 1 of the passenger terminal, right in the heart of the city! It's a much more convenient location, with plenty to do right around the gate, and many transportation options readily available.

My division had watch today, so I didn't get to leave the ship. However, I'll be headed out tomorrow to experience Puerto Rico, especially the beaches! I haven't decided exactly what I'm going to do, but part of the day will definitely be spent doing nothing on the beach. I can't wait!

That's all I have for now, I want to get some sleep so I'm ready to go tomorrow. I'll get some pictures and stories and post them over the next couple of days.
