---
title: 'Winter Storm Saturn: Annapolis Area Updates and Resources'
date: 2013-03-05T17:56:19.000Z
slug: winter-storm-saturn
image:
  src: /files/blog/winter-storm-saturn/snow-generic.jpg
  alt: Snow

tags:
  - annapolis
  - liveblog
  - snow
  - weather
  - winter storm saturn
---

The Washington, DC area is expecting some snow over the next 24-36 hours, and while accumulations won't be significant (less than 12 inches), it's likely to cause some disruptions. BGE is pre-positioning assets in preparation for power outages, and local emergency management agencies are setting up shelters and posting preparedness reminders. Annapolis & Anne Arundel County aren't likely to see more than a few inches so the impact should be minimal, but it's best to be ready nonetheless.

I'll be posting updates on this page throughout the event.

# Resources

Here are some good resources to keep an eye on:

## Government

- [Maryland Emergency Management Agency](http://mema.maryland.gov/): [Twitter](https://twitter.com/mdmema) and [Facebook](https://www.facebook.com/MDMEMA)
- [Anne Arundel County Office of Emergency Management](http://www.aacounty.org/OEM): [Twitter](https://twitter.com/aaco_oem) and [Facebook](https://www.facebook.com/pages/Anne-Arundel-County-OEM/95865467945)

## Media Outlets

- [NBC4 Washington](http://www.nbcwashington.com/): main [Twitter](http://www.nbcwashington.com/), weather [Twitter](https://twitter.com/nbcwashington/storm4-weather), and [Facebook](https://www.facebook.com/nbcwashingtondc)
- [WTOP](http://www.wtop.com/) 103.5FM: [Twitter](https://twitter.com/wtop) and [Facebook](https://www.facebook.com/wtopnews)
- [Eye on Annapolis](http://www.eyeonannapolis.net/): [Twitter](https://twitter.com/eyeonannapolis) and [Facebook](https://www.facebook.com/naptown)
- [Weather Underground Winter Storm Saturn Information](http://www.wunderground.com/winter-storm/Saturn-2013)

Remember to pick up some extra batteries and provisions, check flashlights, have a battery-powered radio available, and be cautious when driving in snow.

# Live Updates

## <time timedate="2013-03-05T17:58:00+00:00">March 5, 2013 12:58pm</time>

Here's the latest forecast information for the storm:

![24 hour snowfall forecast](/files/blog/winter-storm-saturn/24-hour-forecast.png)

## <time timedate="2013-03-05T18:14:00+00:00">March 5, 2013 1:14pm</time>

Annapolis Patch has a post [detailing emergency procedures, including phone numbers for utilities and local agencies](http://annapolis.patch.com/articles/refresher-snow-emergency-procedures-in-annapolis).

## <time timedate="2013-03-06T03:19:00+00:00">March 5, 2013 10:19pm</time>

The snow hasn't started yet, but it's getting close.

![Saturn radar 01](/files/blog/winter-storm-saturn/radar01.png)

## <time timedate="2013-03-06T03:31:00+00:00">March 5, 2013 10:31pm</time>

Weather Underground's [WunderMap](http://wxug.us/12396) is an excellent tool for tracking the storm.

## <time timedate="2013-03-06T15:07:00+00:00">March 6, 2013 10:07am</time>

No snow so far, just a bunch of wind and rain. It's 39 degrees in Annapolis, so it doesn't seem likely any snow is coming. ![USNA](/files/blog/winter-storm-saturn/usna.jpg)

## <time timedate="2013-03-06T19:14:00+00:00">March 6, 2013 2:14pm</time>

The strong winds are dropping the temperature a bit. We're starting to see a snow-rain mix coming down. Nothing is sticking to the ground.

## <time timedate="2013-03-06T19:28:00+00:00">March 6, 2013 2:28pm</time>

Bay Bridge is now closed in both directions due to the winds from the storm.

## <time timedate="2013-03-06T19:34:00+00:00">March 6, 2013 2:34pm</time>

The latest radar & temperatures. ![Saturn radar 02](/files/blog/winter-storm-saturn/radar02.png)

## <time timedate="2013-03-06T19:37:00+00:00">March 6, 2013 2:37pm</time>

A tractor trailer has overturned on the Bay Bridge, westbound span due to high winds.

[Live traffic camera »](http://www.chart.state.md.us/video/video.asp?feed=b1019e712e53001b00503336c4235c0a)

[![Traffic camera](/files/blog/winter-storm-saturn/traffic-camera.png)](http://www.chart.state.md.us/video/video.asp?feed=b1019e712e53001b00503336c4235c0a)

## <time timedate="2013-03-06T20:35:00+00:00">March 6, 2013 3:35pm</time>

It looks like the worst of the storm has passed. Still no snow.

![Saturn radar 03](/files/blog/winter-storm-saturn/radar03.png)

## <time timedate="2013-03-06T23:04:00+00:00">March 6, 2013 6:04pm</time>

The wind has died down, and the temperature seems to be steady. Larger pockets of snow on the radar than before, but still none near Annapolis.

![Saturn radar 04](/files/blog/winter-storm-saturn/radar04.png)

## <time timedate="2013-03-07T05:33:00+00:00">March 7, 2013 12:33am</time>

The wind is gone, and it appears the storm is also.

![Saturn radar 05](/files/blog/winter-storm-saturn/radar05.png)