---
title: BMW Plans to Cease Production of M3
date: 2010-04-01T14:29:13.000Z
slug: bmw-plans-to-cease-production-of-m3
image:
  src: /files/blog/bmw-plans-to-cease-production-of-m3/no-m3.jpg
  alt: No more M3

tags:
  - "april fool's"
  - bmw
  - bmw m3
---

BMW announced today that they'll be cancelling their iconic high-performance vehicle, the M3\. The company plans to end production of the car at the end of this year, and has no plans to restart production or roll out a new version.

Citing declining sales and a poor economy with an unsure future, along with an increased focus by the general public on fuel efficiency and government regulation to enforce it, the company says it's not financially feasible to continue developing & producing the sports saloon. They'll continue to produce the current model for racing, but no word on whether a next generation M3 will be made available for the motorsport circuits.

In other news, April Fool's. Sorry, couldn't help myself :-D
