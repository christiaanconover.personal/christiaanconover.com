---
title: "I'm Dumping Flickr"
date: 2011-10-04T16:00:31.000Z
slug: dumping-flickr

tags:
  - flickr
  - photography
---

I've been a [Flickr user for a few years](http://www.flickr.com/photos/christiaanconover/), but over the past year or so I've barely given it more than a glance. It's not for the lack of a quality service - Flickr works very well for posting pictures online and allowing other people to see them and comment. It's due to Flickr not meeting my needs or wants for photo sharing anymore, through a lack of some features, the level of photography that it seems to draw, and the lack of my community of people with whom I engage.

Just to be clear, I'm not deleting the pictures I've already put there. I don't believe in removing content from the web that's been publicly available. That's akin to throwing out public records - it's a piece of history, and you never know when it might be needed. I'm simply stating that I will no longer be paying for Flickr Pro, and won't be posting any new pictures there.

The single biggest reason I'm leaving Flickr behind is the features, or lack thereof. When I first started using Flickr, I uploaded every single image I had stored on my computer. My thought process was that, at the time, I had no backup of my pictures, and putting them on Flickr would ensure that they were safe and resistant to any loss from crashes or failures on my computer. As a result, I started deleting old pictures from my computer to make space. I didn't think it through though, because Flickr has **no native support for exporting your data**. This in itself is a deal-breaker for me. If I can't get out my own pictures from a service in a more efficient way than downloading each picture one by one, I'm not going to support that service. They're my pictures, what right do they have to prevent me from getting them back? I understand that Flickr isn't meant to be a backup solution, but at the same time they should allow you to download your own pictures in a simple process. They don't, so I'm not giving them any more of my stuff. They also don't have very good Android support. As an Android user, this is a pretty big obstacle. Up until recently there wasn't even an official Flickr app for Android. I downloaded their app a couple of days ago, and tried to log in. This is what I was met with:

![Flickr for Android Login Failure](/files/blog/dumping-flickr/flickr-android-error.png)

I can't log in, because their login page doesn't exist. So much for putting pictures from my phone on Flickr.

I don't take a lot of pictures, and when I do they're mostly of things that are either informal and not very interesting, or they're of personal events and things with friends & family. In other words, they're not exciting to people that don't know me. The trend on Flickr seems to be that it's a place for professional or amateur photographers to share their work, or for companies to share imagery from their endeavours. That's not the kind of photography I do, so it's not much benefit to me to post my pictures there.

That brings me to my other reason: the community. My community hangs out on Twitter, Facebook, Google+, etc. I can post pictures easily to all of those places through either their respective photo sharing services or an integrated third party - or even through a link to my own site. Flickr doesn't really benefit me in any of those situations.

Currently I use [Picasa](http://picasa.google.com/) as my primary photo manager. It integrates beautifully with [Picasa Web](https://picasaweb.google.com/), Google's competitor to Flickr, and that in turn integrates seamlessly with my phone. Combine that with the recent features added through Google+, and anything Flickr may have had over Picasa before is pretty much gone for me.

So that's my Flickr situation. Time to move onto the next photo thing.