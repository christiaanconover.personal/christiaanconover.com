---
title: "What I'm Reading"
date: 2013-08-08T15:23:57.000Z
slug: what-im-reading-14
image:
  src: /files/blog/what-im-reading-14/pier-sunset.jpg
  alt: Sunset behind a pier

tags:
  - anil dash
  - smoking
  - "what i'm reading"
---

[**Shushers: Wrong About Movies. Wrong About the World.**](http://dashes.com/anil/2013/08/shushers-wrong-about-movies-wrong-about-the-world.html)<br>
Stop freaking out when somebody uses a phone in a theater. You're fighting a losing battle.

[**Why Smokers Still Smoke**](http://www.nytimes.com/2013/07/28/opinion/sunday/why-smokers-still-smoke.html)<br>
Hint: it's genetics.

[**Why I'll Be a Solo Founder Next Time**](http://dennybritz.com/blog/2013/08/05/why-i-will-be-a-solo-founder-next-time)<br>
Start your company alone. Only add the right people.
