---
title: "What I'm Reading"
date: 2013-07-03T15:31:49.000Z
slug: what-im-reading-6
image:
  src: /files/blog/what-im-reading-6/sunset-in-college-park.jpg
  alt: Sunset in College Park

tags:
  - "what i'm reading"
---

[Rising Early: Why Successful People Do It & How You Can Too](http://www.ericosiu.com/rising-early/)

[Stop Penalizing Boys for Not Being Able to Sit Still at School](http://www.theatlantic.com/sexes/archive/2013/06/stop-penalizing-boys-for-not-being-able-to-sit-still-at-school/276976/)

[Why Healthy Eaters Fall for Fries](http://www.nytimes.com/2013/06/30/sunday-review/why-healthy-eaters-fall-for-fries.html)

[Why Everyone Should Telecommute](http://www.pcmag.com/article2/0,2817,2421263,00.asp)

[Progressive enhancement is still important](http://jakearchibald.com/2013/progressive-enhancement-still-important/)
