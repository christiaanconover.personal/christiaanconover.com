---
title: 'The Latest: Updates from Around Campus'
date: 2008-03-22T05:21:55.000Z
slug: the-latest-updates-from-around-campus

tags:
  - amateur radio
  - inspections
  - mass maritime
  - sailing
---

I'm sorry it's been a week since my last post. I haven't written anything due to a combination of being wicked busy, and not having much to write about (those two don't really seem to go together, do they?). At any rate, I thought I'd post an update on some of the more interesting things going on with me, as well as around campus.

By now everyone has sorted out their schedule, and has gotten into their routine with classes and extracurriculars. Sailing has started for the spring semester, so we're practicing in the afternoons, and will soon start competing. Practice was canceled this past week on Wednesday and Thursday due to the weather, but next week we should be able to pick back up.

I'm working on getting an amateur radio club established at MMA. As a ham radio operator, I'd like to find other cadets who are either operators already, or are interested in becoming licensed, to expand the hobby within the school. Not only do I think it can be a lot of fun, but it also has practical application in the maritime industry, especially for deckies. I'll post more on that as we make progress in establishing the club.

Since Easter is on Sunday, we don't have to be back at school until "mofo" (short for morning formation) Monday morning, which means we have no regimental inspections this week! For somebody outside the school, this may not seem like a big deal, but it's huge in the eyes of a freshman, since it means you can spend another night at home and not have to worry about being back in time to prepare for inspections. It essentially adds on Sunday afternoon and night to your weekend.

I'll check back in on Monday; right now I need to get back to laying low this weekend.
