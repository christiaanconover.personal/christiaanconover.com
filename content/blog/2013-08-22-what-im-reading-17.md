---
title: "What I'm Reading"
date: 2013-08-22T14:41:54.000Z
slug: what-im-reading-17
image:
  src: /files/blog/what-im-reading-17/lightning-cliffs.jpg
  alt: Lightning over the cliffs

tags:
  - censorship
  - police
  - trey ratcliff
  - "what i'm reading"
---

[How to build your personal brand: The next step to anything](http://thenextweb.com/entrepreneur/2013/08/18/your-personal-brand-the-next-step-to-anything/)

[The Digital Divide Is Still Leaving Americans Behind](http://mashable.com/2013/08/18/digital-divide/)

[Censorship Doesn't Just Stifle Speech -- It Can Spread Disease](http://www.wired.com/opinion/2013/08/ap_mers)

[Drawing Down: How To Roll Back Police Militarization In America](http://www.huffingtonpost.com/2013/08/15/how-to-roll-back-police-militarization_n_3749272.html)

[Cracking suicide: hackers try to engineer a cure for depression](http://www.theverge.com/2013/8/14/4618718/hacker-depression-def-con)
