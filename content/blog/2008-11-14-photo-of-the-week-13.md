---
title: 'Photo Of The Week #13'
date: 2008-11-14T13:32:48.000Z
slug: photo-of-the-week-13
image:
  src: /files/blog/photo-of-the-week-13/qe2.jpg
  alt: Queen Elizabeth 2

tags:
  - dubai
  - mass maritime
  - photo of the week
  - queen elizabeth 2
  - southampton
---

This week, the [Queen Elizabeth 2](http://en.wikipedia.org/wiki/RMS_Queen_Elizabeth_2) visited [Southampton, England](http://en.wikipedia.org/wiki/Southampton) for the last time ever. The cruise liner is making its final voyage before coming to rest in [Dubai, United Arab Emirates](http://en.wikipedia.org/wiki/Dubai) to live out the rest of her days as a floating hotel and museum. You may have heard in the news that she ran aground on her way into Southampton, but sustained no damage. She departed Southampton with fireworks and ceremony, a proper farewell for a ship of such notoriety. The picture I chose for this week's [Photo Of The Week](/tags/photo-of-the-week/) is of the QE2 as she heads out of Southampton, back out to sea on her way to Dubai.