---
title: '"Celebrate Your Car" Day: 328i Edition!'
date: 2010-03-28T15:51:19.000Z
slug: celebrate-your-car-day-328i-edition

tags:
  - '#cardate'
  - 328i
  - bmw
  - celebrate your car
---

It's another "[Celebrate Your Car](/tags/celebrate-your-car/)" day here on the blog! This time we're celebrating all varieties of the BMW 328i. With the first 328i made available in 1994 as an [E36 body](http://en.wikipedia.org/wiki/BMW_3_Series_(E36)) it's been a fine member of the 3 series family ever since.

You know the drill: if you're the proud owner of a 328 of any style or year, post a link to a picture of it in the comments below, and maybe add a description, or even an expression of adoration (we are BMW folks after all, that's how we roll!) and share the love of this great car.

**Full disclosure:** this is actually a back-dated post after the [initial one that gave me the idea](/blog/today-is-celebrate-my-car-day/) was put up, and I realized it wouldn't be fair for 328 guys to have to wait a whole 363 days for their celebratory day!

**Let's see those 328s!**
