---
title: First Day (and Night) at Sea; Arrival in Norfolk
date: 2008-01-14T02:40:03.000Z
slug: first-day-and-night-at-sea-arrival-in-norfolk

tags:
  - mass maritime
  - sea term
  - sea term 2008
  - shaft alley
---

After leaving Buzzards Bay, we went back to work. For Division 1, that meant resuming watches. For the other divisions, it meant maintenance. I had watch from 1530-1930, After which I spent some time relaxing on deck, and then went to bed.

The Engine Room is pretty interesting while we're under way. It's incredibly loud and hot, but there is a ton of cool machinery to look at. To me, the coolest part of it was watching the shaft spin. The shaft is completely exposed, so you can see it spinning, knowing that on the other side of the aft bulkhead in Shaft Alley is the propeller, and the ocean. I have also come to realize that when a ship is under way, you truly can walk to and from somewhere uphill both ways.

Today is Sunday, so we have Sunday at Sea. Sunday at Sea is our day to relax and hang out. Watches still continue, but for anybody not actually on watch at a given time, they are free to do as they like. I, for one, slept until 1300 (not intentionally). When I woke up, I found people sleeping, watching movies, playing XBOX (yes, somebody in my hold brought an XBOX with them), reading, etc.

We are currently coming in to Norfolk harbor, about to pass over the Chesapeake Bay Bridge-Tunnel. I think I'll go out and take some pictures of our entry into the Bay, and discuss it in my next post.

<video controls="">
  <source src="/files/blog/first-day-and-night-at-sea-arrival-in-norfolk/shaft-alley-tour.mp4" type="video/mp4">
</video>