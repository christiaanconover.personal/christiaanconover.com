---
title: Bunkering in Norfolk
date: 2008-01-15T00:30:03.000Z
slug: bunkering-in-norfolk

tags:
  - mass maritime
  - sea term
  - sea term 2008
---

As I mentioned at the end of yesterday's post, we pulled into Norfolk around lunch time yesterday. I lived in Norfolk for a few months in 2000, so it was really cool to see from the water some of the places I'd been to when I lived in the area. We anchored in Norfolk harbor not too far from the Naval base, so it's been pretty exciting to have aircraft carriers, destroyers and the like as the scenery while we eat in the mess deck, or relax on the fantail. First Division is the watch division for Monday, Tuesday and Wednesday, and I have been assigned engine room watch 0000-0400 and 1200-1600\. Engine watch for freshmen consists of tracing out various systems, such as the fuel oil and main steam systems. The Engine portion of our Sea Term grade consists of two parts: labs done in the engine training space, and learning systems in the engine room. All of the watches I have stood on Sea Term so far have been Engine watches, so I've had the good fortune to get extra time to learn and study the systems. Tomorrow we'll be headed back out to sea, en route to our first port of call in Panama! Excitement is growing each day, and I think it will seem even more real once we get to warmer climate. I, for one, can't wait to go through the Panama Canal! That's all many days off though, and watch is just a few short hours away, so I think I'll go get some sleep.
