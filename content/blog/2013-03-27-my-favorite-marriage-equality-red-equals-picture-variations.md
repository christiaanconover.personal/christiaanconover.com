---
title: 'My Favorite Marriage Equality "Red Equals" Picture Variations'
date: 2013-03-27T18:41:47.000Z
slug: my-favorite-marriage-equality-red-equals-picture-variations
image:
  src: /files/blog/my-favorite-marriage-equality-red-equals-picture-variations/red-equals.jpg
  alt: Red equals

tags:
  - memes
  - red equals
  - same-sex marriage
  - scotus
---

This week the U.S. Supreme Court is hearing a number of cases relating to same-sex marriage & marriage equality. To show support for those affected by the decisions on these cases, many people have been displaying an image of a red square with a pink 'equals' symbol in the center as their Facebook, Twitter, etc. profile picture. This being the Internet, a number of riffs off the original image have been created which run the gamut from jokes to social commentary.

Here's a collection of my favorites that I've come across:

![Marriage equality expressed with bacon...because everybody loves bacon.](/files/blog/my-favorite-marriage-equality-red-equals-picture-variations/bacon.jpg)<br>
Marriage equality expressed with bacon...because everybody loves bacon.

![Paula Dehn's cooking includes lots of marriage equality.](/files/blog/my-favorite-marriage-equality-red-equals-picture-variations/butter.jpg)<br>
Paula Dehn's cooking includes lots of marriage equality.

![Admiral Adama supports marriage equality.](/files/blog/my-favorite-marriage-equality-red-equals-picture-variations/adama.jpg)<br>
Admiral Adama supports marriage equality.

![This one speaks for itself.](/files/blog/my-favorite-marriage-equality-red-equals-picture-variations/statue-of-liberty-lady-justice.jpg)<br>
This one speaks for itself.

![The nine Supreme Court justices embedded in equality.](/files/blog/my-favorite-marriage-equality-red-equals-picture-variations/scotus.jpg)<br>
The nine Supreme Court justices embedded in equality.

![Those immortal words from the Declaration of Independence.](/files/blog/my-favorite-marriage-equality-red-equals-picture-variations/life-liberty-happiness.jpg)<br>
Those immortal words from the Declaration of Independence.

![Because sloth.](/files/blog/my-favorite-marriage-equality-red-equals-picture-variations/sloth.jpg)<br>
Because sloth.

What are your favorites?
