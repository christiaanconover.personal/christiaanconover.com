---
title: 'Shellbacks!'
slug: shellbacks
date: 2008-02-08T08:50:03.000Z

tags:
  - mass maritime
  - sea term
  - sea term 2008
---

Today was the shellbacking ritual, which was wicked cool! Unfortunately, the details are secret and exclusive to Shellbacks, so I can't really go into them here. However, I can say that it was an awesome day! We are now steaming along to Panama, to go back through the canal and return to the Atlantic, and head to Aruba! Everyone's really excited to get to Aruba, since it's been the port most people have been looking forward to. I have watch again tonight 0000-0400 and tomorrow 1200-1600\. I'll check in again tomorrow afternoon.
