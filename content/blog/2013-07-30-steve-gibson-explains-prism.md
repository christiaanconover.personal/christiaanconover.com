---
title: Steve Gibson Explains How PRISM Works and Where the Name Comes From
date: 2013-07-30T16:43:59.000Z
slug: steve-gibson-explains-prism

tags:
  - leo laporte
  - prism
  - security now
  - steve gibson
---

[Steve Gibson](https://grc.com), the security guru and co-host of [Security Now!](http://twit.tv/sn) with Leo Laporte, did an episode a few weeks ago in which he explains how PRISM works without direct access to companies' servers. He's determined that the process involves upstream intercepts at ISPs and major Internet companies like Google, using fiber optic splitters - hence the name PRISM.

The details are very interesting, and worth a listen.

{{< youtube fX8CSMPiTs4 >}}