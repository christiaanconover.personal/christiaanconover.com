---
title: 'Photo of the Week #10'
date: 2008-06-14T00:04:58.000Z
slug: photo-of-the-week-10
image:
  src: /files/blog/photos-of-the-ts-enterprise/enterprise01.jpg
  alt: T.S. Enterprise at sunset

tags:
  - mass maritime
  - photo of the week
  - ts enterprise
---

You probably thought I'd forgotten about the Photo of the Week this week, didn't you? Not to worry, I had simply been outside enjoying the day and was waiting to post it when I thought other people would be back inside too. I know it's a Friday evening in June, but there may be a few people reading it tonight, you never know.

Anyway, this week's picture is actually one I took last fall after being at the Academy for only a few weeks. It's probably one of my top three favorite photos that I've taken of the Enterprise so far. I really like the way the sun hits the ship, making everything light up & glow.

I'll try to continue this segment for the next few weeks until [I go to boot camp](/blog/uscgr-enlisting/), but since school will be out next Tuesday it'll have to all be old material like this one.
