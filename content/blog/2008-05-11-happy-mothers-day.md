---
title: "Happy Mother's Day!"
date: 2008-05-11T06:36:49.000Z
slug: happy-mothers-day

tags:
  - "mother's day"
---

Happy Mother's Day to all the moms out there!

I wouldn't be anywhere close to where I am today, and headed for such a bright future, if it hadn't been for my mom. She's the most loving, compassionate, and generally amazing person I know. I can only hope than anybody reading this has a mother as fantastic as mine is.

![Mom and Me at my High School Graduation](/files/blog/happy-mothers-day/graduation.jpg)
