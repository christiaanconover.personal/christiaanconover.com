---
title: Chart Work - The Core of Learning For a Young Deckie
date: 2008-09-25T03:37:48.000Z
slug: chart-work-the-core-of-learning-for-a-young-deckie

tags:
  - coastal navigation
  - marine transportation
  - mass maritime
---

I received a suggestion last week (I know, I really need to post more often/faster) that I explain what chart work is. It took me a little bit to understand that this suggestion is valuable in a couple of ways. First off, I'm guessing that whoever suggested it is not the only reader of mine that doesn't know what that term refers to, so it would behoove me to explain it. Second, it reminded me that I'm not writing this blog for an audience of maritime cadets, or licensed mariners, but for people who are either connected to Mass Maritime, or the maritime industry, without necessarily knowing any specifics about it. So, thank you to whoever posted that suggestion.

Anyway, what is chart work? Well, if you ask any freshman or sophomore deck cadet here at MMA, they'll probably tell you it's a frustrating pain in the @$!# to do. Chart work is short-hand for the homework assignments we receive in Coastal Navigation that involve plotting courses and other navigational evolutions on a chart. A chart, for those who may not know, is sort of like a map, but for the water. It shows the depth of a body of water at various points in the water, the shipping channels for that area, buoys, lights, and landmarks along the shore.

A typical chart work assignment can take anywhere from 1-3 hours (if you work steadily). It can end up taking much longer if you work in groups, as is often the case, because invariably conversation moves very far away from chart work, and stays there for a while. So, an assignment that is time-consuming on its own can become even more time-consuming, though it's completely the fault of the people doing the chart work (I'll be the first to admit that I have that problem).

Once you get the hang of whatever concept the assignment is teaching, chart work can be fun. I'd equate it to that feeling that some people (not me) who enjoy math get from solving a challenging problem. It's time-consuming, but it's satisfying once you understand it and finish it.
