---
title: Patriotism and Free Speech
description: Nationalism is not patriotism.
date: 2015-02-26T16:19:59.000Z
slug: patriotism-and-free-speech
image:
  src: /files/blog/patriotism-and-free-speech/rally.jpg
  alt: Rally

tags:
  - 1st amendment
  - nationalism
  - patriotism
---

I read an article this week about a group of high school students in Portland, Maine [questioning the recitation of the Pledge of Allegiance](http://www.pressherald.com/2015/02/24/freedom-of-speech-backlash-of-anger-over-four-words-at-high-school/) in school. The TL;DR is that the senior class president, responsible for the morning announcements, has added the phrase "if you'd like to" after her request for students to rise and recite the Pledge. The students taking up this cause have a better understanding of patriotism, free speech and their rights as Americans than any of their detractors, including the school's faculty and staff.

> "It has nothing to do with our patriotism. We really want to enlighten our students. We believe they should be learning what the pledge actually means and choosing (whether) to say it."

> _Lily SanGiovanni, South Portland High School senior class president_

The reaction to their position is symptomatic of a larger, disturbing trend I've seen in the wake of 9/11: the conflation of patriotism and nationalism. If somebody is behaving in a way that goes against our notion of Americanism, they are labeled unpatriotic. Making statements against our military means you hate freedom. Claiming that the United States is not the greatest country on Earth garners an invitation to go live somewhere else, as you're no longer welcome here. The fervor of nationalism that has developed in the past fourteen years has caused many of our citizens to lose sight of what our rights under the First Amendment really mean.

{{< tweet 570301342033559552 >}}

It has long been my contention that those who voice dissenting opinions stand in the ranks of the true patriots. The most powerful way you can show your love of our free speech rights is to say the unpopular thing, the thing nobody wants to hear. Those students at South Portland High School are the embodiment of what it means to be a United States citizen, and any educator worth a damn should be holding them up as the example. That they are sparking a debate in their community is an added bonus. They are questioning why, and asking others to do the same and make their own choice.

My father is a retired U.S. Army officer, and for a portion of my childhood we lived on post. When evening colors was sounded and I was playing on the playground with the other kids, we would all stop what we were doing, stand and face toward the flag. Nobody had told us to do so, but we had seen our parents do it, and I would like to think we had some understanding of why. That memory sticks with me not because it was a demonstration of respect for the flag and those who had fallen to defend it, but because as children we made the decision, on our own, to be part of that moment of respect. It was our choice.

I am fortunate to have parents who instilled in us the respect and appreciation for our rights as Americans that has shaped the perspective I have today. They placed heavy emphasis on asking questions, and seeking knowledge rather than simply accepting somebody else's opinion as truth. I suspect that those students at South Portland High have similar good fortune, and I hope their efforts will make a difference with their fellow students. My hat is off to them for being true patriots.