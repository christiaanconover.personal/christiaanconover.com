---
title: Kindle Fire Software Update 6.2.1
date: 2011-12-23T04:36:33.000Z
slug: kindlefire-update-621
image:
  src: /files/blog/kindlefire-update-621/update00.jpg
  alt: Kindle Fire Update

tags:
  - amazon
  - gadgets
  - kindle fire
---

Amazon released an update for the Kindle Fire today aimed at fixing some of the bugs and interface frustrations people were complaining about. The update brings the Fire's software to version 6.2.1 and so far does seem to be a noticeable improvement. Touch responses are far more accurate, and everything feels snappier in general.

If your device doesn't automatically prompt you to update, you can do it manually by [downloading the file from Amazon](http://www.amazon.com/gp/help/customer/display.html?nodeId=200790620). Their instructions state that you need to download the file to your computer, and then plug your Kindle in to copy it over, but this isn't necessary. All you need is a file manager such as [EStrongs File Explorer](http://www.amazon.com/EStrongs-Inc-ES-File-Explorer/dp/B004HN2FY0/ref=sr_1_1?ie=UTF8&qid=1324614894&sr=8-1). Once you click download on Amazon's [page for the software update](http://www.amazon.com/gp/help/customer/display.html?nodeId=200790620), the file will be downloaded to your device.

![Downloading Kindle Fire Software 6.2.1](/files/blog/kindlefire-update-621/update01.jpg)

The file will be saved, conveniently enough, in the 'Download' folder which you can easily access using a file browser like EStrongs.

!["Download" Folder on the Kindle Fire](/files/blog/kindlefire-update-621/update02.jpg)

Inside you'll find the file for the software update.

![Kindle Fire Software 6.2.1 Update File in the 'Download' Folder](/files/blog/kindlefire-update-621/update03.jpg)

Long press it, and click 'Cut' on the menu.

![EStrongs 'Cut' Dialog](/files/blog/kindlefire-update-621/update04.jpg)

Navigate up a level, and go into the 'kindleupdates' folder. Press the gray tab at the bottom of the screen, and hold & drag the file into the folder, and choose 'Paste' when prompted. The file is now in the 'kindleupdates' folder ready to be applied.

![Kindle Fire Software Update in 'kindleupdates' Folder](/files/blog/kindlefire-update-621/update05.jpg)

Click the Home button, and then on the status bar at the top tap the Gear icon, then tap 'More' on the right, and tap the 'Device' entry on the menu. You should now see the 'Update your Kindle' button lit up.

![Kindle Fire 'Update your Kindle' Button](/files/blog/kindlefire-update-621/update06.jpg)

Tap it and let the device do the rest.

![Kindle Fire Updating](/files/blog/kindlefire-update-621/update07.jpg)

Once it finishes rebooting and takes you back to the lock screen, you're all set! If you go back into the 'Device' menu, you'll notice that the Kindle now segments the storage space between apps and everything else. I'm not entirely sure why they did that, and since I'm not a big app person I'm not thrilled that they're reserving space I'm not likely to fill with apps, but oh well. For app-heavy users, I wonder if they'll hit a ceiling more quickly. We'll have to wait and see.

![Kindle Fire 6.2.1 Memory Meters](/files/blog/kindlefire-update-621/update08.jpg)

One of the main issues I've been having with mine has been that the Wifi would drop its connection to the access point, but it wouldn't show that and would think it was still connected. The only way to fix it was to turn Wifi off and on, and there didn't seem to be much pattern. I haven't spent enough time with the update yet to know whether this is fixed.

All in all, it does seem like a good improvement. Be sure to get your update as soon as possible.

<video preload="metadata" controls="">
  <source src="/files/blog/kindlefire-update-621/software-update.mp4" type="video/mp4">
</video>