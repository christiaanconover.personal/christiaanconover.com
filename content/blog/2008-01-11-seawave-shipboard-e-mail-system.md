---
title: SeaWave Shipboard E-mail System
date: 2008-01-11T19:10:03.000Z
slug: seawave-shipboard-e-mail-system

tags:
  - mass maritime
  - sea term
  - sea term 2008
  - seawave
---

I have set up my SeaWave e-mail account on board the Enterprise, and thought I would test it to make sure I was able to post blog entries with it. It also seemed like a logical time to post a few of the key points that we've been continuously reminded of over the past few weeks regarding e-mail.

The e-mail system charges by how much data the e-mail account sends AND receives. Therefore, things like pictures, music, and any other attachments will significantly increase the total cost of the e-mail system. DON'T SEND ATTACHMENTS OF ANY KIND UNLESS YOU WANT TO PAY A LOT OF MONEY TO SEAWAVE.

E-mail on board the ship is not instant. The system connects via satellite to the shore-side e-mail server, and only connects a couple of times a day. Therefore, it may be many hours until you get a reply from your cadet. It's not that he or she is ignoring you, there's simply an inherent delay in the system.

E-mail service availability is at the discretion of the ship's officers. If it is deemed a logistics or security problem, they may turn off the e-mail server for cadets on board the ship. Again, if it's a day or two since you've received an e-mail from your cadet, it's not necessarily because they're ignoring you.

I'm sure that most of this information has found its way to parents, but I thought I'd reiterate it anyway. Less than 24 hours to go!
