---
title: 'Case-of-the-Mondays Question: Engine Time'
date: 2010-04-05T15:16:22.000Z
slug: case-of-the-mondays-question-engine-time
image:
  src: /files/blog/case-of-the-mondays-question-whats-your-all-time-favorite-bmw/case-of-the-mondays.jpg
  alt: 'Case of the "Mondays"'
  link: '/tags/case-of-the-mondays/'

tags:
  - '#BMWBunch'
  - '#BMWMonday'
  - bmw
  - case of the mondays
  - e46
  - engine
  - bmw m3
  - s54
---

For this week's "Case-of-the-Mondays" question, we get a bit more technical. Last week was a simple question of what your favorite BMW vehicle is. This week I'd like to drill a bit deeper into the build of BMWs, and focus on engines.

**What do you think is the best production BMW engine of all time?** This includes ///M engines and engines sourced from BMW by other manufacturers for use in production/road cars. Like last week, this is likely to be a very tough decision, but give it a shot anyway.

![BMW S54](/files/blog/case-of-the-mondays-question-engine-time/s54.jpg)

My choice for this question is the [S54B32](http://en.wikipedia.org/wiki/BMW_S54B32). Used in a number of high-performance BMW models, most notably the E46 M3, this engine set the standard for the naturally aspirated inline 6\. At 343bhp for the European version and 333bhp for the U.S. it set records for power per liter, and won numerous awards for best engine. Truly a work of art as much as an amazing piece of engineering, it's testament to BMW and ///M to build truly amazing vehicles and powerplants.

**Which engine is your favorite?**