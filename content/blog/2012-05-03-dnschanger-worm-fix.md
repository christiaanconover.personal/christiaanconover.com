---
title: The DNSChanger Worm and You
date: 2012-05-03T16:23:50.000Z
slug: dnschanger-worm-fix

tags:
  - cloudflare
  - cyber security
  - dns
  - dnschanger
  - opendns
---

A couple of years ago some hackers started distributing a quirky little virus that would change the [DNS servers](http://en.wikipedia.org/wiki/Domain_Name_System) your computer uses. The people responsible were [located by the FBI and their data gathering system was terminated](http://www.fbi.gov/news/stories/2011/november/malware_110911). Despite this you could still be infected, and affected, by the virus.

For those unaware, [DNS stands for Domain Name Service](http://en.wikipedia.org/wiki/Domain_Name_System), and is the system that tells your computer where a web address is located. For example, when you type [google.com](http://google.com) into your browser, the computer asks its DNS servers at what [IP address](http://en.wikipedia.org/wiki/IP_address) that site is located, and once it has that information you're sent to Google. Without that information, you can't get anywhere.

After the FBI shut down the rogue DNS servers the virus pointed your computer to, they set up their own servers at the same address so people would still be able to surf the internet if they'd been infected. On July 9th, 2012 the FBI will be shutting down these servers, leaving millions of users potentially stranded without explanation.

**Here's how it affects you**: your computer may very well be infected without you knowing. It's important to determine this and remove it if you are infected. If you do have the virus you should have received a message about it when you came to this page. [Check yourself if you really want to be sure](http://www.dns-ok.us/). If you are infected, the [DNSChanger Working Group has information on removing it](http://www.dcwg.org/fix/).

*Whether you're infected or not, I'd still recommend you take steps to set your DNS servers to OpenDNS. They have great service that offers additional security online, and it's free. You can [get additional information about them](http://opendns.com), and [test their security features at their demo page](http://www.internetbadguys.com/).*