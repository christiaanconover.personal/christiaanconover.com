---
title: Why I Use VLC Media Player
date: 2008-01-20T01:30:09.000Z
slug: why-i-use-vlc-media-player

tags:
  - videolan
  - vlc
---

Greetings from somewhere in the Caribbean Sea! I'm on Mass Maritime's annual training cruise aboard the T.S. Enterprise, and we're currently steaming through the Caribbean on our way to Panama. On board, when we're not busy with maintenance or training, we spend a lot of time watching movies. I keep my movies on a Western Digital 500GB external hard drive, and I have quite a large collection that I'm always looking to increase. Videos that other people have are often in a variety of different formats. Luckily, VLC plays all of them. I first discovered VLC (also called VideoLAN) about two years ago, in the package repo for the Linux distro I was using at the time. I tried it out, and was immediately impressed. In addition to supporting every media format I threw at it, VLC can also transcode and stream natively. It pretty much appeared to be an all-in-one solution for my video needs. I have found the transcoding feature to be especially handy on board the ship. I keep all the videos on my hard drive in DivX format, so it is sometimes necessary to convert a video I get into this format. VLC makes this really easy, with the wizard. It makes it equally as easy to convert videos from DivX to other formats, such as iPod-compatible MPEG-4\. We have a wireless network on board that allows us to access the shipboard e-mail system, as well as the Internet when we have a shoreside connection. I haven't tried it yet, but I plan to test out the streaming feature on our network to see how the bandwidth is. I'm planning on posting a few basic tutorials on how to do some of my most frequently used features in VLC within the next few days. Stay tuned!
