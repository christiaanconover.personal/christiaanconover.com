---
title: 'Rougher Seas Make Classes a Lot More Fun!'
date: 2008-01-20T01:30:09.000Z
slug: rougher-seas-make-classes-a-lot-more-fun

tags:
  - mass maritime
  - sea term
  - sea term 2008
---

Today we've had much larger waves than the past few days. The ship has done quite a bit of rolling, and we're starting to understand much better why the tables in the mess deck have slide guards on the edges! I've been in MSEP training since Thursday, and today was the last day. The chairs and stools in the classrooms are not fixed to the deck in any way, so when the ship rolled we all went sliding to one side or the other. There were a few times when everyone in the room ended up sliding against the bulkhead in a big pile of chairs and people! Tomorrow is Sunday at Sea, and First Division has a half day of maintenance in the morning. We're the maintenance division Monday through Wednesday as well, so we'll see what's in store for us. Tomorrow, however, we're just going to relax.
