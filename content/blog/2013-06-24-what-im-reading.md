---
title: "What I'm Reading"
date: 2013-06-24T22:15:57.000Z
slug: what-im-reading
image:
  src: /files/blog/what-im-reading/phone.png
  alt: Phone

tags:
  - ecuador
  - edward snowden
  - grilling
  - hadoop
  - instagram
  - nsa
  - "what i'm reading"
---

[**NSA Concedes Hadoop Beats Its Pricey Alternatives**](http://readwrite.com/2013/06/21/nsa-concedes-hadoop-beats-its-pricey-alternatives#awesm=~o9I0fh6cxSWSoM)<br>
The National Security Agency has acknowledged that Hadoop, the open source big-data analysis tool, is more powerful and higher performing than its commercial counterparts.

[**The Poke Test, Using a Fork to Flip, and Other Steak-Cooking Myths**](http://lifehacker.com/the-poke-test-using-a-fork-to-flip-and-other-steak-co-513292207)<br>
Lifehacker tackles how to properly cook a steak, just in time for grilling season.

[**Why Would Ecuador Want Edward Snowden?**](http://www.npr.org/blogs/parallels/2013/06/23/194957079/why-would-ecuador-want-edward-snowden)<br>
An explanation of possible reasons Ecuador is considering granting asylum to the former NSA contractor.

[**How Video Gives Instagram A Split Personality**](http://readwrite.com/2013/06/20/instagram-video-identity-crisis#awesm=~o9IfA1G1370LHS)<br>
I'm not an Instagram user, but this is an interesting discussion about why adding video could be a real problem for the future of the service.
