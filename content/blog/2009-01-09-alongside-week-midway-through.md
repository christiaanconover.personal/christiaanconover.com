---
title: 'Alongside Week: Midway Through'
date: 2009-01-09T03:02:31.000Z

tags:
  - ais
  - ecdis
  - gps
  - inspection
  - mass maritime
  - sea term
  - sea term 2009
  - us coast guard
---

It's Thursday night, and we've only got two full days left in Buzzards Bay before casting off lines and heading south. We're doing very well in preparations for departure, with only a couple of major things left on the to-do list. On-load was finished today, with the arrival of 200 gallons of milk. We're now fully stocked and ready to feed 600 people fully for the next 49 days.

Tomorrow is our Coast Guard-monitored fire/emergency and abandon ship drills. We have to be approved by the Coast Guard on our response for those drills in order to be legally allowed to sail. The Coast Guard vessel inspection team has been on board throughout the week going over various parts of the ship to ensure that they are safe and in compliance with regulations. Tomorrow is the last phase of that. In the morning we'll have a series of "dress rehearsal" drills, and then at 1300 we'll have the actual monitored drills. Hopefully they go well!

We also received news that all underclass cadets have been waiting for: liberty will be granted this week! Freshmen will be receiving liberty tomorrow, Friday, from 1800-2300\. Sophomores will be receiving liberty on Saturday from 1600-2200\. If any freshman or sophomore is scheduled to have watch during that time, they will stand their watch and have their liberty on the other day during the same scheduled time. For example, if a freshman has watch 2000-0000, they will stand their watch tomorrow, and then have liberty on Saturday from 1800-2300\. Check with your cadet to find out about their liberty situation.

In personal news, our cadet IT department on board the ship (of which I am a member) completed a major goal today. For the past month, we have been working on a project to place TVs and monitors throughout the ship to display the picture from the ECDIS on the bridge. ECDIS stands for Electronic Chart Display Information System, and is an integrated system that connects many different electronic navigation systems such as GPS, chartplotting, AIS, and others into a single, easy-to-read picture. It displays an electronic real-time chart, along with projections of surrounding vessels, and our own position and navigation data. Today, we connected the penultimate (and in my opinion, most important) of the remote monitors: the one in the cadet mess. We connected the ones in the crew lounge, the engine room, and the Master's office yesterday. However, this is the one that we will get to enjoy. It's going to be a very nice tool to have available to us so we can get a better sense of our location and surroundings out at sea, as well as a good teaching device. So, a personal victory for myself and the rest of the IT department!

We will be departing Buzzards Bay at 0900 on Sunday, 11 January 2009\. The ship will be open for guests and visitors 0600-0730 that morning (I know, it's really early, but that's the only time we had available), which is a great chance to see the environment in which your cadet will be living for the next six weeks. I would highly recommend taking advantage of this opportunity to share the experience with your cadet even more fully.

I have to sign off for now and try to grab a little bit of sleep before I go onto watch at 2330, which I will be standing until 0330\. I'll check in tomorrow, most likely after the Coast Guard emergency drills.
