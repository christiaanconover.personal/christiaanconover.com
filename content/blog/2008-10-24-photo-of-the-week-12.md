---
title: 'Photo of the Week #12'
date: 2008-10-24T13:08:23.000Z
slug: photo-of-the-week-12
image:
  src: /files/blog/photo-of-the-week-12/ts-kennedy.jpg
  alt: T.S. Kenndey

tags:
  - mass maritime
  - photo of the week
  - ts enterprise
  - ts kennedy
---

I took this picture this morning from my room, which is why the picture quality leaves a little to be desired. However, I wanted to seize the opportunity to get the shot, since they're keeping the new name covered up until the ship is christened as the USTS Kennedy. So, enjoy this "sneak preview" picture of Massachusetts Maritime Academy's training ship with its new nametag.

If you're a little confused as to why there's a new name on the bow, [take a look at this post](/blog/training-ship-name-change/).
