---
title: Fall is the Monday of Seasons
description: "Fall sucks. There's no two ways about it."
date: 2013-09-06T23:35:34.000Z
slug: fall-is-the-monday-of-seasons
image:
  src: /files/blog/fall-is-the-monday-of-seasons/case-of-the-mondays.jpg
  alt: Case of the Mondays

tags:
  - curmudgeon
  - fall
  - halloween
  - rant
---

I hate fall. It's the worst season of the year, bar none. Never mind that my birthday is in October, or that birthdays of close friends and family are in fall, or that football is back, or that my utility bill is lower. None of that matters, because fall is still a shitty season. Let's count the ways fall is awful.

### It's the Monday of the year.

I have a theory that the cycle of months and seasons throughout the year can be described by days of the week. Fall is Monday because it shows up at the end of summer to remind you that outdoor fun and more than 6 hours of daylight are over until you can start shedding layers in 7 or 8 months, so you might as well just settle in and ride it out. December through March are Tuesday and Wednesday, because the only difference between those months and the fall months is that it's a little colder and you've already resigned yourself to them, but at least you're making progress toward the weekend. April is Thursday and you're finally closing in on summer. May is Friday morning & afternoon - the fun is _almost_ back and you're already thinking about all the good times ahead. June is Friday night, July is Saturday, and August is Sunday - all of which should be self-explanatory. Then we're back to September to remind us it's Monday again.

Why does fall get an entire day to itself when summer gets squeezed into barely two and a half? Because summer, like the weekend, is never long enough and goes by way too quickly - while fall never freakin' ends and seems longer and worse than spring and summer combined.

Oh, and one more reason fall is Monday: you have to deal with the cheery folks who love fall and wait all year for cool air and leaves of all different shades of death. The only thing worse than summer ending is those who are psyched about it.

"Isn't this just the _best_ time of year?" say they. "Choke on a scarf," say I.

### The weather is atrocious.

Fall's weather is like that annoying friend who can never make up his mind, and when he finally does it's just as annoying, but you can't get rid of him. One day it's cold and rainy, the next day it's warm and sunny, and just when you think it may not be so bad it turns to cold and sunny. The one thing you can count on is the ever-decreasing amount of sunlight that, by the end of October, guarantees you'll be at work during the few hours the sun actually decides to grace us with its presence. Once November (or as we established earlier, Monday night) shows up, you've all but forgotten what it's like to wake up on a warm, sunny Saturday chock full of potential for spontaneous backyard cookouts and comfortably sitting outside in a t-shirt and boxers.

Even the few days that feel a lot like summer are still annoying, because they're _not quite_ summer weather and they only serve to remind you of exactly that.

### Halloween sucks.

Yes, I'm curmudgeonly enough to put a rant inside a rant, so let's do this. Halloween is a stupid holiday if you're over the age of 14\. When you're a kid Halloween is awesome because you get to dress up like a super hero or a knight and have wildly elaborate recess games, and possibly a break from class for a costume parade. Then it's kicked up a notch when you get home and go trick-or-treating, because it's the one time a year you get to collect a shitload of candy from all your friends' parents and stuff your face without getting yelled at. When you're an adult everything that used to be great about Halloween disappears. There's no time off from work to parade around like an asshole in a goofy outfit - that's the express train to unemployment. If I want to stuff my face full of two pounds of candy, I'll just go buy some candy from the store. I'm a grown man, nobody will yell at me for it - except my intestines, who will kindly remind me I can't pull that crap anymore. The one redeeming element of Halloween is the 20-something girls who have an excuse to dress in skimpy outfits and are celebrated for it. However, you also have to have a costume to take advantage of that, which is a major pain in the ass. Screw Halloween.

### No, pulling out coats and hats is not "fun" and shut up about it.

In case it hasn't been made clear thus far, I hate dressing in layers. My ideal weather is when I can get out of bed and walk straight outside in my underwear and be perfectly comfortable. I'm not saying I do that very often (I'm also not saying I don't do it), but that's about the point at which I think "yup, this is working for me." If I need to start adding articles of clothing to transition from indoors to outdoors, I'm less than thrilled. However, if I need to add layers in the morning, then remove them during the day, then put them on again during the day, then remove them again, then put them on again, then take them off, then put them on at night - well, now I'm perturbed.

### Fall sucks.

Sure, there are fun things that happen September-November. No, I'm not going to move to a place without seasons - I really like where I live, and the weather is one very small component of my day-to-day. Still, it's a factor and fall is a bummer. I'll stick to burgers and beer in the sun, and you can keep your goddamn coat.

*This post was written with the voice of [Lewis Black](https://en.wikipedia.org/wiki/Lewis_Black) in my head, and so is best read that way. If you read it with a different voice, it would be best to go back and read it with his.*