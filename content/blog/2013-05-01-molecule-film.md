---
title: We Live in the Future.
date: 2013-05-01T14:59:57.000Z
slug: molecule-film

tags:
  - atom
  - ibm
  - molecule
---

IBM has managed to make a stop-motion film using - wait for it - **individual molecules**. Yes, you read that correctly. They have managed to not only capture, but control, the movements of molecules to create a short film. This is blowing my mind.

They provide [an explanation of how the movie was made](https://youtu.be/xA4QWwaweWA), which is nearly as cool as the film itself. It apparently took 10 days of working 18 hours a day to create the frames for the minute-long video, using microscopic magnets and sound waves.

Once again let me reiterate: this is a movie in which the characters are created using individual freakin' molecules.

{{< youtube oSCX78-8-q0 >}}