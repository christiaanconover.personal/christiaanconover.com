---
title: 'Beer Picks: Yuengling Porter'
date: 2011-03-24T22:07:10.000Z
slug: beer-picks-yuengling-porter
image:
  src: /files/blog/beer-picks-yuengling-porter/6-pack.jpg
  alt: 6 pack of Yuengling Porter

tags:
  - beer picks
  - porter
  - yuengling
---

I've decided to start a new segment on my site. It's called [Beer Picks](/tags/beer-picks/) and the concept is simple: ~~each week~~ from time to time I'll pick a beer that either I've had before and like, or one I've never had before but looks interesting, and feature it on the site. I'll describe it as best I can, which will be pretty basic since I'm by no means a beer expert. I simply like beer and enjoy sharing ones I like with others, so I'm going to give this a shot.

![Yuengling Porter](/files/blog/beer-picks-yuengling-porter/pint-glass.jpg)

To kick off this new segment, I'm going with one of my long-time favorites: Yuengling Porter. You'll discover, I'm sure, as I do more posts for this that I'm a big fan of porters & darker beers. This was one of the first porters I ever tried and really gave me a taste for them. It's a rich, full-bodied porter without any specialty flavoring. If you like porters, you'll like this.

If anyone has suggestions on beers for this series, throw them in the comments.