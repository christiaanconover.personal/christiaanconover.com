---
title: Amazon Still Has a Long Way to Go to Beat Netflix
description: "Amazon is great at a lot of things. So far, video streaming isn't really one of them."
date: 2014-03-13T14:07:44.000Z
slug: amazon-still-has-a-long-way-to-go-to-beat-netflix

tags:
  - amazon
  - amazon instant video
  - netflix
  - streaming
---

I'm a big fan of Amazon. I'm dependent on my Prime membership for everything from deodorant to Christmas gifts, and at ~~$79~~ [$99](http://www.theverge.com/2014/3/13/5503908/amazon-prime-price-increases-to-99-per-year-in-us) a year it's a bargain. I've been impressed with their Instant Video original content efforts, and their ability to get big names in television and movies to license programming to them. Despite that, they're still a long way from convincing me to leave Netflix and go all in on Amazon.

### Be Everywhere

Netflix has gone to lengths to make sure that wherever you are, whatever device you're using, you can stream from them. It doesn't matter whether you have an Android device, iOS device, Mac, PC, Roku, Xbox, or even one of a myriad of smart TVs on the market. If it has a screen and the internet, you can probably watch Netflix on it. They've gotten so good at this, that on the rare occasion you encounter something that won't stream Netflix it's a surprising disappointment.

Amazon has made no such effort. Sure, you can stream from them if you're on a computer, but beyond that your options are more limited. Watching on your phone means having an iPhone; Android need not apply. It's a similar situation with tablets: iOS yes, Android no - unless you have a Kindle Fire, which naturally have full support for Amazon streaming. Their availability on larger screens is growing, but still pales in comparison to Netflix.

### Simplicity Wins

Starting a movie or TV show on Netflix is as simple as clicking on it. Picking up right where you left off on a movie or TV show is as simple as clicking on it. The Netflix experience makes the time between opening the site or app and watching what you came for as short as possible. If I want to watch something on Amazon, I have to go to the Instant Video section of the site, then search for what I'm looking for, then determine whether it's Prime-eligible or not, then (if it's a TV show) figure out which season or episode I want...I'm already annoyed because I've had to take too many steps to get where I was trying to go. God forbid I want to resume watching something I stopped earlier, especially if I'm going between devices. Amazon's ability to pick up where you left off is spotty at best, and completely non-functional most of the time.

### Too much work, too little competitive edge

For a company known for their fanatical drive to put as little friction as possible between the customer and the product, Amazon's Instant Video offering is uncharacteristically cumbersome and inaccessible. The experience has frustrated me with such regularity that now, if a show I want is only available on Amazon, I have second thoughts before committing to watching it there, and may decide to wait until it ends up on Netflix. Until Amazon gets serious about matching Netflix's industry-gold-standard experience, they won't be taking my business from our red friend.