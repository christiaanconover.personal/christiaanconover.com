---
title: A Look Behind the Curtain at Koenigsegg
date: 2012-06-08T15:51:29.000Z
slug: koenigsegg-driven
image:
  src: /files/blog/koenigsegg-driven/koenigsegg.png
  alt: Koenigsegg
tags:
  - drive
  - jf musial
  - koenigsegg
---

The term "supercar" typically conjures images of Ferraris, Lamborghinis, and the rest of the extravagant, outrageous and incredibly expensive Italian and German machinery that most people only dream of. The term "hypercar" is harder to pin down, perhaps because it's a relatively new concept. Covered by this term is a Swedish company that manufactures some of the fastest, most powerful and most technically marvelous vehicles in the world - that most people have never heard of.

One of my favorite ~~Internet~~ shows, [DRIVE](https://www.youtube.com/DRIVE), did an episode of their [DRIVEN](https://www.youtube.com/playlist?list=PL829BEC349EA4F0E6&feature=plcp) segment this week that's a behind-the-scenes look at [Koenigsegg](http://www.koenigsegg.com/). Host JF Musial takes us on a tour of the factory on a decommissioned military base near the Swedish coast with company founder and owner [Christian von Koenigsegg](http://en.wikipedia.org/wiki/Christian_von_Koenigsegg). JF also takes a ride in an Agera R on Koenigsegg's test track, driven by one of their test drivers (the driver is 24 years old and has the best job on the planet).

The video is about 30 minutes long, but it's absolutely worth watching. Even if you aren't big into cars, the process for building some of the most exclusive machinery on earth is amazing to watch.

{{< youtube qp_qxKWMsVw >}}
