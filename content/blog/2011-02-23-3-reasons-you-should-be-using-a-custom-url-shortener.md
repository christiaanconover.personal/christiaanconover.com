---
title: 3 Reasons You Should Be Using a Custom URL Shortener
date: 2011-02-23T20:35:44.000Z
slug: 3-reasons-you-should-be-using-a-custom-url-shortener
image:
  src: /files/blog/3-reasons-you-should-be-using-a-custom-url-shortener/url-shortener.png
  alt: URL shortener

tags:
  - bit.ly
  - cnvr.cc
  - url shortener
---

We've all seen them: links in somebody's Facebook or Twitter post to something through [bit.ly](http://bit.ly), [goo.gl](http://goo.gl), [j.mp](http://j.mp), etc. These services are a great way to consolidate big, long, ugly links into something more manageable & space-saving. However, they're also fairly anonymous. You've probably also seen short URLs that are associated with a certain brand or company, such as Amazon's [amzn.to](http://amzn.to), The New York Times' [nyti.ms](http://nyti.ms), or even Christiaan Conover's [cnvr.cc](http://cnvr.cc). There are some definitive reasons for doing this, and if you have a site & use short URLs to share its content you should look into having your own custom short URL.

Bear in mind as well, in the age of new media you as an individual can be, and realistically are, a brand. As you're building or maintaining yourself as an online entity, you'll want to be taking advantage of every tool you can to make yourself recognizable.

# Brand Identity & Recognition

If you're simply using the standard bit.ly links to share pages, your links have no more meaning to somebody seeing it in their feed than anyone else. Sure, they'll carry some weight when they're in posts from your accounts, but if somebody else shares the link they'll look just like any other link. If you have a custom URL, people will instantly recognize that the link they're going to click on comes from your brand. If you have a reputation for quality content worth viewing (which hopefully you do), then people may be more likely to click through to the page when they see a link from your short URL. The link will continue to carry that reputation for you when it starts showing up in places beyond the accounts you control, further extending your reach to existing & new visitors.

# Brand (and link) Trust

This one is perhaps the most important factor in my mind. One of the biggest problems with generic short URLs is that you have no idea what a link goes to unless you click on it. If you're trying to build positive relationships with readers & visitors to your site, why would you want to give them a link that they may not be comfortable clicking on? Sure, it's probably safe since you're posting it on your account, but how can they know? Plus, the benefit of the doubt is gone when it's being posted in places outside of your control.

By using a short URL, you're doing a couple of things. First, you're giving potential visitors confidence that the link they're about to click on is trustworthy. You're also letting them know that you endorse whatever page you're linking to. This can be a double-edged sword, if the page changes after you've linked to it & suddenly isn't what you intended it to be, but that's rare & not much of a concern. Since you alone have access to the account for your custom URL shortener, users will know that a page linked to through your custom shortener domain have been vetted & won't harm their computer, which **builds trust not only in your links but in your brand**. You're growing a stronger relationship with users at every level.

# It Looks Better

This one is obviously more subjective, but I think it's got enough validity to include it. When you share a link with somebody using your custom short URL, it gives the impression that you know what you're doing. For folks who are more technically savvy & understand what a link shortener is, they'll appreciate that you're more clued in to how it works too & that you're committed to fully cultivating your online presence. For people who aren't as Internet wise, you'll appear more professional (which actually applies to the former group as well) and they'll probably take you a little more seriously.

Think of it like your own domain for your email & web site. If your email address is `yourcompanyname@gmail.com` you'll look small & not so professional, no matter what your size or credentials. If, however, your email address is `yourname@yourcompanyname.com` you now look like a fully professional, large-scale operation. Your web site works the same way and nowadays **your link shortener does too, since it represents your brand**.

# Luckily, It's Easy To Do

For my URL shortening I use Bit.ly Pro. It's the same as standard Bit.ly but it allows you to have a custom URL, for free. You still have to buy the domain you want to use (the fewer the number of characters, the better), but the actual shortener service is free. Bit.ly will walk you right through how to set it up, so it's not a big deal at all. Once you have everything configured, Bit.ly will automatically shorten any link you give to it using your domain. Plus, if somebody uses Bit.ly to shorten a link from your web site, it will automatically give them a short URL with your domain as well, to ensure that any Bit.ly link associated with your site looks that way.

If you run your own web site, I'd highly recommend taking advantage of this. It's a great & easy way to ensure that your links are recognizable anywhere they're posted, and builds invaluable brand equity.
