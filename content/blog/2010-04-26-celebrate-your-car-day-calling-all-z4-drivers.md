---
title: '"Celebrate Your Car" Day: Calling all Z4 Drivers!'
date: 2010-04-26T14:48:11.000Z
slug: celebrate-your-car-day-calling-all-z4-drivers

tags:
  - '#cardate'
  - bmw
  - bmw z4
  - celebrate your car
---

Since the Z series vehicles don't follow the standard BMW naming convention, I had to come up with a way to accomodate them. So, since "Z" is the 26th letter of the alphabet they'll be celebrated on the 26th day of their respective number month. So since today is the 26th day of the 4th month, we're celebrating the Z4! Sounds a little complicated, I know, but it's really pretty simple & makes sense when you think about it.

So let's see those Z4s! I don't happen to own one (yet :-D) but it's one of my favorite cars, let alone roadsters. Whether you have a first or second generation Z4, we want to see them all! Maybe include a story about it, but definitely show it off.

**Happy Z4 day!**
