---
title: My New Car
date: 2010-01-20T21:04:28.000Z
slug: my-new-car
image:
  src: /files/blog/my-new-car/natalia.jpg
  alt: Natalia

tags:
  - 330ci
  - bmw
  - e46
  - natalia
  - my cars
---

I picked up a new (to me) car on January 9th. I apologize about the posting delay, I kinda forgot to do it. Anyway, it's a 2001 BMW 330ci, black with black leather interior, sport package, and generally excellent shape. I found it on the BMW CCA classifieds section, for an intriguingly low price. I emailed the seller asking for more information, and the reply was better than I could have ever hoped for. The seller told me that this was his favorite Bimmer in a long line of them, starting with a 1969 1600 he bought new. He was the original owner on this car as well, and was only selling it to buy an '05 330 ZHP with low mileage for a price he couldn't turn down. Everything I asked him regarding maintenance, common issues, etc. all got replies with answers that were both positive and demonstrated his thorough knowledge and love of the vehicle. It was located in Vermont, so I made the arrangements to go up and get it, & my uncle came along. The car was even better than I expected when I first saw it in person, and drives like a dream. It's higher mileage but it's clear that the miles on the car are no indication of the condition, as it looks & feels like a car with a quarter of the miles it has. Plus, I got an excellent deal which even included a set of winter wheels & tires! I'm truly thrilled, and as my first experience with the E46 3 series I'm absolutely loving it.
