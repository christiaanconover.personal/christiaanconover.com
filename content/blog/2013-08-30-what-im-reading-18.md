---
title: "What I'm Reading"
date: 2013-08-30T15:44:45.000Z
slug: what-im-reading-18
image:
  src: /files/blog/what-im-reading-18/college-park-creek.jpg
  alt: College Park creek by Andy Feliciotti

tags:
  - bitcoin
  - edward snowden
  - foursquare
  - "what i'm reading"
---

[The Bitcoin of governance could be coming soon](http://qz.com/116136/the-bitcoin-of-governance-could-be-coming-soon/)

[Edward Snowden's Real Impact](http://www.newyorker.com/online/blogs/comment/2013/08/edward-snowdens-real-impact.html)

[Will Foursquare CEO Dennis Crowley Finally Get it Right?](http://www.fastcompany.com/3014821/will-foursquare-ceo-dennis-crowley-finally-get-it-right)

[The Best Hangover Cure](http://www.slate.com/articles/life/drink/2013/08/hangover_cure_pedialyte_freezer_pops_are_more_pleasant_than_a_saline_solution.html)

[Climate Name Change](http://climatenamechange.org/)
