---
title: 'My New Callsign: W3RTX'
date: 2011-11-30T18:00:00.000Z
slug: new-callsign-w3rtx

tags:
  - amateur radio
  - w3rtx
---

As of yesterday, I have a new callsign: [W3RTX](http://wireless2.fcc.gov/UlsApp/UlsSearch/license.jsp?licKey=3334417). My original callsign, [KB3MMY](http://wireless2.fcc.gov/UlsApp/UlsSearch/license.jsp?licKey=2739157), is no longer in use. You'll notice that all amateur radio content on this site now falls under W3RTX.

Why the change? Perhaps the biggest reason was that I just wanted a vanity callsign. It's goofy, I know, but I thought it would be cool to pick my own callsign. I also wanted one that wouldn't indicate when I got licensed, and that I'm relatively new to ham radio.

When it came time to decide what I wanted my new call to be, I had a few criteria already figured out. First, I wanted to keep the '3' area since that's where I live. I also wanted a 'W' prefix call, because I just like the way it sounds on the radio, both normally and phonetically. I also wanted a call whose letters don't sound like any other letters. For example, my old call had the letter 'M' in it twice. The letter M can easily be mistaken for the letter N when saying it over the radio, which could cause confusion. I wanted to get as close as I could to a call with letters that sound totally unique.

I made a list of ones I liked that fit my criteria, and settled on W3RTX because in addition to it meeting all the requirements, also kind of looks like 'RX' and 'TX' (receive and transmit) are right in it, which made it extra cool for radio use.

So there you have it, the story behind my new callsign. The letters have no meaning beyond themselves, I just like the way it looks and sounds. If you hear it on the air, give me a shout!
