---
title: Outdoor Shower Time
date: 2010-07-21T21:13:52.000Z
slug: outdoor-shower-time
image:
  src: /files/blog/outdoor-shower-time/shower.jpg
  alt: Outdoor shower on Boston Island

tags:
  - boston island
  - maine
---

Our shower rig out on the island. Water is in a shower bag we put out in the yard to heat in the sun all day, and holds 6 gallons.
