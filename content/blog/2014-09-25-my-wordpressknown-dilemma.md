---
title: My WordPress/Known Dilemma
description: Known is awesome. WordPress still has my heart.
date: 2014-09-25T14:30:37.000Z
slug: my-wordpressknown-dilemma

tags:
  - blogging
  - indieweb
  - known
  - wordpress
---

I'm a huge WordPress fan. that's no secret. I've been using it since 2007 for pretty much every site (blog or otherwise) that I've built. I write plugins and themes for it. I've fully invested my personal website and blogging experience in the WordPress ecosystem.

That's why, when I learned about [Known](http://withknown.com/) a couple of weeks ago, I found myself in a bit of a dilemma. Known seems to embody the same principles and goals as WordPress: federated content, control of what you publish, open source codebase, distributed community, the list goes on. Known provides an answer to an itch that I, and I'm sure many others, have had for a while about WordPress: it feels heavy. WordPress is carrying around the weight of legacy code support, and an API style that feels a little old. Meanwhile, its core capabilities have been expanding from simple blogging platform to full-blown CMS. In some ways I love that, since I can use my beloved WordPress for just about anything I want to build. In other ways, I feel like I'm brining a semi truck to a job for a pickup. Known is new, and purpose-built for blogging in the modern era. It's written from the ground up for the [IndieWeb](http://indiewebcamp.com/) movement. It's more Tumblr than WordPress. It feels fresh.

[I have a Known site on my server](https://stream.cconover.com/) and I've been playing around with it here and there. So far I'm impressed with its simplicity and ease of use. The only reason I'm not totally in love with Known is that I feel like I'm cheating on my WordPress site. It's the same feeling I had when I experimented with Tumblr a few years ago (though that was worse, because Tumblr is a data silo).

I'm excited to see the IndieWeb movement gaining momentum, and software being built specifically for that purpose. I'll likely find a place for Known in my processes, and I'll certainly encourage others to use it. I just love WordPress too much to leave it entirely.
