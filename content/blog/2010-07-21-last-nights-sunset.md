---
title: "Last Night's Sunset"
date: 2010-07-21T21:31:57.000Z
slug: last-nights-sunset
image:
  src: /files/blog/last-nights-sunset/sunset.jpg
  alt: Sunset over the Sheepscot River

tags:
  - boston island
  - maine
---

The sunset as seen from Cocktail Rock, the top of the island overlooking the Sheepscot River.
