---
title: "What I'm Reading"
date: 2013-07-25T15:04:24.000Z
slug: what-im-reading-13
image:
  src: /files/blog/what-im-reading-13/island.jpg
  alt: Random island

tags:
  - ars technica
  - depression
  - gigaom
  - new york times
  - prism
  - the atlantic
  - the verge
  - trey ratcliff
  - "what i'm reading"
---

[The Vitamin Myth: Why We Think We Need Supplements](http://www.theatlantic.com/health/archive/2013/07/the-vitamin-myth-why-we-think-we-need-supplements/277947/) [The Atlantic]

[When Prisoners Protest](http://www.nytimes.com/2013/07/17/opinion/when-prisoners-protest.html) [New York Times]

[Everything You Need to Know About PRISM](http://www.theverge.com/2013/7/17/4517480/nsa-spying-prism-surveillance-cheat-sheet) [The Verge]

[Why I Hate Read Receipts](http://arstechnica.com/staff/2013/07/why-i-hate-read-receipts/) [Ars Technica]

[Raising the Wrong Profile](http://www.nytimes.com/2013/07/19/opinion/coates-raising-the-wrong-profile.html) [New York Times]

[Existential Depression in Gifted Children](http://theunboundedspirit.com/existential-depression-in-gifted-children/) [The Unbounded Spirit]

[What I Learned About Digital Addiction by Going Swimming with My Cellphone](http://gigaom.com/2013/07/23/what-i-learned-about-digital-addiction-by-going-swimming-with-my-cellphone/) [GigaOm]
