---
title: 'ChristiaanConover.com Has Moved Into a New, Faster Home'
date: 2011-12-22T23:35:09.000Z
slug: new-server-mtve

tags:
  - media temple
---

You may have noticed when you came to this page that the site is loading a lot faster than it has in recent weeks. I've moved to a new server - one that's all mine! Ok, so I don't own the actual hardware, but I purchased a virtual server that gives me dedicated resources that I don't have to share with anyone else. The result, combined with better software that takes advantage of the new server's abilities, is a much faster and more reliable site.

That said, as I tweak things to work perfectly, there may be a glitch here and there. If you encounter something weird, please let me know so I can resolve it.

Mi casa e su casa, so feel free to whip around here at new-found breakneck speeds :-)

For those who are curious, the site is now running on a [Media Temple (ve) 512mb](http://mediatemple.net/webhosting/ve/). I've been a [huge fan of Media Temple](/blog/media-temple-service-rocks/) since I switched to their (gs) service earlier this year, and wanted to stick with them as I made my upgrade.
