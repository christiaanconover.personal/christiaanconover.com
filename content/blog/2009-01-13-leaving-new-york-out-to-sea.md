---
title: 'Leaving New York, Out to Sea'
date: 2009-01-13T05:15:09.000Z
slug: leaving-new-york-out-to-sea

tags:
  - mass maritime
  - sea term
  - sea term 2009
---

As I'm writing this we're headed due south out of New York Harbor, passing Barnegat Inlet. We're making about 15 knots, and the seas are relatively calm. I'm laying in my rack, being gently rocked by the ocean, which is an awesome sleep inducer. However, I'm determined to make a blog post and fire off a few e-mails before falling asleep.

Today marked the first day of rotations on cruise. There are four rotations, which allows each division to be on a different rotation at any given time. Division 4, my division, had Training today. For freshmen, that meant either Deck, Engine, or MSEP classes. For 3rd and 1st class it meant classes for their major. I had class about voyage planning, signal flags, and weather observing. As a 3rd class deck cadet, I have to complete a group voyage plan project, for which we'll be making a complete voyage plan for Sea Term this year, following all the requirements and guidelines of a proper voyage plan. It's worth 30% of our final cruise grade, so it's important that we do a good job on it.

We spent today in New York taking on fuel, known as bunkering. We burn a fuel called Bunker C, which is basically one step above crude oil, hence the term bunkering. Now that we've got a full tank, it's time to hit the road. We got underway around 1900 this evening, and proceeded south under the Verazzano Bridge and along the New Jersey shore. I would guess that we'll be passing Cape Hatteras Tuesday night or Wednesday morning, which should be exciting. It's typically a rougher area of ocean, since it's where the Gulf Stream interacts with the colder waters of the North Atlantic near the coast. I'm looking forward to hopefully experiencing some rougher seas (I know, call me crazy, but I want to experience it anyway). That's all I have for now. I'll update on SPOT when I'm able to, so keep an eye out.
