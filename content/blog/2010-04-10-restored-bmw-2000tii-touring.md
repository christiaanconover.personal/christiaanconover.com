---
title: Restored BMW 2000tii Touring
date: 2010-04-10T05:14:45.000Z
slug: restored-bmw-2000tii-touring
image:
  src: /files/blog/restored-bmw-2000tii-touring/touring01.jpg
  alt: 1972 BMW 2000tii Touring

tags:
  - 2000tii
  - bmw
  - bmwcca
  - ncc bmwcca
  - touring
---

This past Thursday I was at a social for the [BMW Car Club of America](http://bit.ly/cylmWj) [National Capital Chapter](http://bit.ly/cGCdsx). Now that the weather's getting nicer the turnout is getting larger, the more exotic/classic Bimmers are showing up. This week was one I'd never seen before: a [1972 2000tii Touring](http://bit.ly/a1mh7A). It was fully restored, complete with Colorado orange paint and is absolutely gorgeous. Done as a project by the owner to keep his mind off his son who was fighting in Iraq at the time, the car was completely overhauled to original quality. In fact, the only original body metal on the car is the roof, and front & rear valence panels - everything else either came directly from BMW or was taken from other cars. The car is truly a work of art & a classic - and a rare one at that! More photos after the break.

On a side note, for you BMWCCA members who live in the DC area, the [National Capital Chapter](http://nccbmwcca.org) holds socials the first three Thursdays of each month, at rotating venues. Hang out in the parking lot from 6-7 to admire each others' cars, then go inside whatever restaurant we're at that week & eat. Club picks up $20/head for food. Lots of fun and a good way to meet other enthusiasts.

![1972 BMW 2000tii Touring](/files/blog/restored-bmw-2000tii-touring/touring02.jpg)

![1972 BMW 2000tii Touring](/files/blog/restored-bmw-2000tii-touring/touring03.jpg)

![1972 BMW 2000tii Touring](/files/blog/restored-bmw-2000tii-touring/touring04.jpg)

![1972 BMW 2000tii Touring](/files/blog/restored-bmw-2000tii-touring/touring05.jpg)

![NCC BMWCCA](/files/blog/restored-bmw-2000tii-touring/cca01.jpg)
