---
title: 'Finals Week: Almost Done, but Not Quite Yet'
date: 2008-12-15T13:10:56.000Z
slug: finals-week-almost-done-but-not-quite-yet

tags:
  - final exams
  - mass maritime
  - sea term
  - sea term 2009
  - ts kennedy
---

This week marks the beginning of the end for fall semester. Today is the first day of final exams, bringing mixed emotions to campus. Certainly everyone's excited that in a mere 5 days we'll be leaving to go home for the holidays with school totally out of mind (for those of us not going on Sea Term that's even easier). Morning formation is the shortened formation every morning this week to allow for more studying time. However, obviously there is some anxiousness as cadets are taking tests that can decide singlehandedly what grade they get in a class, or in some cases whether they pass or fail.

For those of us going on [Sea Term](/tags/sea-term-2009//), we're also starting to give serious thought to this journey. I'm sure the freshmen are especially preoccupied by this, as I was last year.

Good luck to everyone on finals, and especially to those cadets whose finals will be deciding their ability to go on Sea Term!