---
title: 'Chrome OS & the Cr-48: A Follow-Up After Months of Use'
date: 2011-05-24T03:10:36.000Z
slug: chromeos-cr48-followup
image:
  src: /files/blog/cr48/boot.jpg
  alt: Cr-48 booting up

tags:
  - chrome os
  - cr-48
  - gadgets
  - google
---

For the past six months I've used the [Cr-48 I got in December](/blog/cr48/) on a daily basis. As you may recall, the Cr-48 is a test computer Google created and sent to chosen applicants to field test their new [Chrome OS](http://en.wikipedia.org/wiki/Google_Chrome_OS). I received one, and I've used it all the time. Since Google just announced that [Chromebooks will start being sold commercially on June 15](http://www.google.com/chromebook), I thought it was time I talked about my experience using it.

# Hardware - The Cr-48 Itself

I want to get this taken care of right up front. I'm only going to spend a few sentences on this since the Cr-48 itself will never be sold, and was only developed for testing. I really like the device, though it's sometimes a bit on the slow side. If you're using a really rich web application like [Seesmic Web](http://seesmic.com/products/web) it can lag a bit, and YouTube videos can get choppy if you play them above 360p. None of this is really a deal-breaker, and 98% of things I do on it never have any speed or performance issues. When it was first shipped a lot of people griped about issues with the touchpad, but those have pretty much all been resolved with software updates. I still really like the matte black soft touch finish on the case, though it's started to wear off in a few spots, like some of the corners. Battery life is still fantastic. All in all, I'm still very much a fan of the Cr-48, and bring it with me almost every time I leave the house.

# User Interface

Let's go ahead and jump into the important part: the software. Chrome OS, from a user interface standpoint, is really no different from the Chrome browser you can install on any computer now. The entire interface is just the Chrome browser, so there's really nothing new to learn here. Really, the only place the user interface differs is logging into the computer, which is simply a box with your username and (if you want) a picture to go with it, and a prompt for your password. Once you log in, you're looking at Chrome.

# Settings & Preferences

Since it's Chrome, all settings are the same as the normal Chrome browser. Plus, since Google has built full profile synchronization right into Chrome any bookmarks, extensions, web apps, etc. that you sync on your current computer will automatically show up on your Chromebook, and vice versa. There's really not much to discuss here, since you already know 95% of it if you use Chrome.

# Networking

This is a crucial part of any Chromebook, since the entire platform is Internet-dependent. The Cr-48 is equipped with 802.11 a/b/g WiFi, as well as a Verizon 3G chip. The 3G works fine, though I use it rarely. WiFi works great, but there are some issues that are software-related. First off, it doesn't support more advanced authentication methods. Many colleges and large companies use a method of connecting to the wireless network that requires entering a username and password during the connection being established. Chrome OS thus far on the Cr-48 doesn't support that, though I have to believe they'll be adding support in time for the June 15 launch. There are also occasional issues with WiFi not reconnecting properly when coming out of standby. That has improved over time, but it's still a problem. Google also recently enabled support for ad hoc networks, such as those created by a phone when doing hotspot tethering.

# File Management

This is an area that still needs work, though it has improved markedly since I first got my Chromebook. Given that Chrome OS does everything on the web and not compute locally, it seems somewhat logical that they would put less emphasis on managing local storage. There are times though when you really need to download a file, or upload one from a flash drive or SD card. I often need to do that for this site, in fact. When I first got the computer, there was barely even a menu entry for accessing files. By now they've added a more complete file manager, and support for external media. However, a lot of basic tasks are still unavailable, such as copying or moving files, or viewing properties. I really would like to see them enhance this part of the OS.

# Security

This is one of the components of Chrome OS that I think really makes it stand out. I've gone to every site I can think of that's a known malware propagator, and haven't had a single issue. It's locked down tight. I tried sideloading a different version of Chrome OS from USB, and it wouldn't take. I'm pretty impressed with how security-minded Google was when designing this platform, and I think it's a great solution for a lot of users, especially in corporate and educational environments where many users may not know best practices for avoiding infection.

# Chrome OS Has Lots of Promise

I've heard a lot of naysayers discounting the idea of an operating system that only does the web. They claim that there's no point, when you can already access the full web on a normal computer, and do a whole bunch of other stuff too. Honestly though, let's think about the average user: most people I know spend the majority of their time on a computer on the Internet. When they're not surfing the web, they're using an office suite, listening to music, or looking at pictures. Very few people I know are doing any sort of actual photo editing, music creation, or video rendering at all, let alone on a regular basis. Even for the developers I know, most of them are doing web development - which you can do very well with some great web apps. The point is, almost every task the average person performs on a daily basis either already is, or can be very easily and comfortably, done online. Plus, most people don't know everything that's required to keep a computer safe and running well, so why give themselves the headache? Chrome OS has the makings of a fantastic platform for most users, and with a few enhancements will be a really solid competitor. I'm looking forward to seeing where it goes, and I'm going to continue using my Cr-48 all the time.
