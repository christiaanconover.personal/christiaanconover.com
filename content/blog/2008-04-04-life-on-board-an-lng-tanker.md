---
title: Life on Board an LNG Tanker
date: 2008-04-04T22:39:43.000Z
slug: life-on-board-an-lng-tanker

tags:
  - lng
  - mass maritime
---

I found this video while surfing YouTube, and thought it would be interesting to share:

{{< youtube CAs7StO-xMs >}}

Looks pretty awesome to me! It certainly provides motivation to do well here.