---
title: 'SPOT on Cruise, Contacting Me'
date: 2009-01-13T05:15:10.000Z
slug: spot-on-cruise-contacting-me

tags:
  - mass maritime
  - sea term
  - sea term 2009
  - spot
---

I thought I should make a quick info post with a few things I've thought of. First off, I will be using SPOT throughout the voyage to provide people back home with position updates in real-time. However, I do not have a place to put it on the ship that would allow it to be operational 24/7, so I'll have to go out on deck and periodically allow it to update. I will make a point of doing this a few times a day, so even though there may not be updates for a few hours, it doesn't mean there's anything wrong.

Second, if you'd like to contact me while we're away, you can do so by using the form on the Contact page of this site. I've set it up to send me an e-mail to my SeaWave address from that form. That allows me to keep the conversation going with people at home, while still ensuring that attachments don't accidentally come through. I welcome any comments or suggestions you have, so please send them my way!

Along those lines, please e-mail me suggestions instead of using Skribit during Sea Term. I know I'm always asking people to do the opposite, but I can't check Skribit out at sea, so e-mailing suggestions to me is the only way to ensure I'll get them.
