---
title: 'New WordPress Plugin: Author Customization'
date: 2013-12-12T15:30:13.000Z
slug: new-wordpress-plugin-author-customization

tags:
  - wordpress
  - wordpress plugins
---

I've released a new WordPress plugin, called [Author Customization](https://wordpress.org/plugins/author-customization/). This plugin gives you much greater flexibility in managing post authors on your WordPress site. Rather than having to rely entirely on the one-size-fits-all user management system, you can customize author names and bio entries on a per-post basis.

You'll also be able to enable a [WYSIWYG editor](https://en.wikipedia.org/wiki/WYSIWYG) for biographical info (the same editor used by WordPress for editing posts), to have richer formatting of author bio entries.

I have big plans for the future of this plugin. I'm working on support for multiple authors on a single post, a seamless guest author process, and more.

Take a look at the [plugin page](https://wordpress.org/plugins/author-customization/) for full details, and give it a try on your site.
