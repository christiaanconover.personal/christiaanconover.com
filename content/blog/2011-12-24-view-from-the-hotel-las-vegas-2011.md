---
title: View from the Hotel
date: 2011-12-24T21:00:19.000Z
slug: view-from-the-hotel-las-vegas-2011
image:
  src: /files/blog/view-from-the-hotel-las-vegas-2011/hotel.jpg
  alt: View from the hotel

tags:
  - vegas 2011
---

The view from our hotel room in Vegas.
