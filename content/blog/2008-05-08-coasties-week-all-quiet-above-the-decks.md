---
title: 'Coasties Week: All Quiet Above the Decks'
date: 2008-05-08T18:51:52.000Z
slug: coasties-week-all-quiet-above-the-decks

tags:
  - license exam
  - mass maritime
  - us coast guard
---

This week the 1st class cadets in license majors are taking their Coast Guard license exams. The exams last all week, and are given in the gym. Since four years of school and training has been leading up to these exams, it's obviously a stressful time for these seniors.

This week is different for all cadets. We have "short mofo" in the morning, which means that instead of forming up across the entire parade field, we form up in a half circle around the patio by the mess deck. This makes morning formation much shorter, so that we don't disturb testing. In the dorms, we're supposed to be quiet to allow the kids taking tests to study. Freshmen have closed door study hours, so we keep our doors shut but have the deadbolt out so that the doors are propped open to allow squad leaders to come in. License seniors are also exempt from all classes this week. A few of my classes are with seniors, so there are a number of people missing.

The school makes efforts to help the license seniors prepare and perform better on the exams. The library has had longer, more accessible hours to allow better studying. There were massage therapists on campus this week to help testers relax, and reduce tension and stress from taking the exams.

1st Company is in a little bit of an awkward situation at the moment, in that we have [Admiral's Inspection](/blog/starting-at-mma-inspections/) next Wednesday. Usually Admiral's is held on a Monday, but they've pushed it back a couple of days to let us prepare after Coasties are over. That said, it usually takes the week before Admiral's to get ready, so we have to work on cleaning in off hours when testing is being done, so we don't disturb the seniors who are studying.

I've found it pretty interesting to watch the Deck seniors as they've been getting ready for, and taking, the Coast Guard exams. Since I'll be in that position in 3 years myself, I've been curious to see how they're handling it. From what I've heard, the Deck tests have gone very well so far.

Good luck to all the seniors in testing right now. Hopefully you all pass with flying colors.
