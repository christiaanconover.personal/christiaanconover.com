---
title: 'Goodbye, Google Voice'
description: "I loved Google Voice and its promise, but I've held out as long as I could for it to realize its potential. It's time to part ways."
date: 2013-12-09T18:46:59.000Z
slug: goodbye-google-voice
image:
  src: /files/blog/goodbye-google-voice/google-voice-logo.jpg
  alt: Google Voice logo

tags:
  - android
  - cell phone
  - google
  - google hangouts
  - google voice
---

I've been a Google Voice user since June 2009, when it was still invite-only and had the promise of doing to phones what Gmail did to email. When I finally got access I was excited, and even though I didn't have a smartphone (which meant using Voice was cumbersome) I was happy with its performance. It was one of the driving factors behind my getting an Android phone, and at first things were awesome.

In the more than four years since I started using Google Voice, very little has been done to improve it or add noticeably lacking features. There is still no support for [MMS](https://en.wikipedia.org/wiki/Multimedia_Messaging_Service), something that I would have expected the likes of Google to figure out long ago. The Android app has gotten worse, not better, with recent updates. The web site hasn't seen any changes in the entire time I've been a user. It's been largely neglected, and it shows. By all appearances, Voice is service for which a death sentence has been given, but the execution has not yet been carried out.

I stopped using my Google Voice number last year, but held onto the voicemail portion. It's time for that to go as well. The few updates Google has made to the Android app in the past year have made voicemail even worse, to the point of complete frustration.

It's disappointing that Google has given so little attention to a service that would logically seem to offer lots of value to their mobile endeavours. With Android 4.0 they touted greater integration with Google Voice in the Dialer app, which to their credit does work well. That's as far as Google Voice has ever made it toward being a mainstream Google product. With their push for Hangouts to become their all-in-one communications platform, it seems inevitable that Voice will fully become the [Milton](http://youtu.be/Vd4fj9Efl4s) of Google products.

So, after some good years and some not-so-good years, I'm parting ways with Voice. Here's to hoping that Hangouts can become everything we hoped Voice would be, but never quite made it.
