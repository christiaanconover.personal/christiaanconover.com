---
title: "I Don't Exist"
date: 2008-06-13T01:58:40.000Z
slug: i-dont-exist

tags:
  - census
  - humor
  - name
---

After [seeing a post](http://tidewatermusings.peterstinson.com/2008/05/how-many-peter-stinsons.html) on [Peter Stinson](http://www.peterstinson.com/)'s blog [Tidewater Musings](http://tidewatermusings.peterstinson.com/), I thought I'd see if there were any other people in the U.S. with my name. As it turns out, not only are there no others with my name, I apparently don't have it either!

![HowManyOfMe.com](/files/blog/i-dont-exist/i-dont-exist.png)
