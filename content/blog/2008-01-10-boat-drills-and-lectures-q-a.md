---
title: 'Boat Drills and Lectures; Q & A'
date: 2008-01-10T21:03:21.000Z
slug: boat-drills-and-lectures-q-a

tags:
  - departure
  - mass maritime
  - sea term
  - sea term 2008
---

The past three days we've done emergency drills every day. We have practiced fire & emergency and abandon ship drills each day, usually taking a total of about one and a half to two hours. Tomorrow we'll be doing the official drills required by the Coast Guard. The Coast Guard inspectors will be on board to ensure that we know exactly how to respond to the different emergency situations. Hopefully things go as well tomorrow as they did before the Orientation Mini Cruise, when the Coast Guard didn't require us to do more than one of the drills since we executed the first one so perfectly.

The fourth class cadets have also spent the past two days in lectures in Admirals' Hall. Most of them have been overviews about the different majors available at Mass Maritime, and a couple of them have been specifically related to Sea Term. We've received information about Marine Safety & Environmental Protection, Emergency Management, and International Maritime Business. We also received a lecture about engine room safety during cruise, which included a slide show of gruesome, but fascinating, shipboard injuries (none of them have occurred at Mass Maritime) to drive home the point of why proper safety procedures are so crucial. This morning we had a briefing with Capt. Rozak and Cdr. Kelleher about proper security procedures in port, and a briefing from the medical staff about proper hygiene and how to go about receiving medical care on board. Freshmen have not had maintenance for the past two days due to these meetings and drills, which has actually been a nice break in the middle of the week.

![Freshmen walk to the T.S. Enterprise](/files/blog/boat-drills-and-lectures-q-a/dsc00972.jpg "Freshmen walk to the T.S. Enterprise")

# Q & A

As I have noted on my contact page, I am happy to answer any questions you may have, or cover any topics you might like to know more about. I received a question from a parent that said the following:

> What time can parents of cadets visit ship on the 12th before departure?

On Saturday, there will be an opportunity for parents to tour the ship. Tours can be done between 0800 and 1000 on Saturday morning. Visitors must be escorted by their cadet, and certain spaces are off-limits to visitors. These spaces are the bridge, the engine room, and the berthing areas. At 1000, all cadets must be on board the ship and all visitors must be back on shore. We will depart at 1150 Saturday morning.

If you have a question or suggestion, feel free to send it to me using the Contact page. I will be continuing to post updates to the blog throughout Sea Term, at least every other day.

We're less than 48 hours from departure. It's really starting to feel real!