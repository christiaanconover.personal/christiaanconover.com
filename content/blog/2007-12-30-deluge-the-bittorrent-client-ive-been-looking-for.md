---
title: "Deluge: The BitTorrent Client I've Been Looking For"
date: 2007-12-30T16:21:48.000Z
slug: deluge-the-bittorrent-client-ive-been-looking-for

tags:
  - bittorrent
  - deluge
  - encryption
  - proxy
---

I've been using Azureus for years as my BitTorrent client, but I've been increasingly dissatisfied with its performance. I often had trouble getting downloads to start (even with no proxies or encryption enabled), and it was hogging system resources it didn't deserve. It's become more bloated with each new release, and it got to the point that I didn't even want to use BitTorrent anymore because my client was frustrating me so much. Then I discovered [Deluge](http://deluge-torrent.org/), and that all changed.

Deluge is a BitTorrent client created specifically to be lightweight, so you won't have to kill your system performance just to download that new Linux distro (c'mon, we all download legal BitTorrent content, right?). It provides all the features that Azureus does (or at least the ones that you need) and you won't even notice it running. I like to maintain my anonymity when downloading torrents, so the proxy and encryption support are vital to me. It's got other cool features that I didn't have with Azureus, such as a built-in anonymous web browser that has dedicated proxy servers to keep you anonymous while you're searching for torrents, and the ability to password-protect the program so people can't just open it from the system tray at a whim and see what you're downloading.

The interface is similar to Azureus, so it makes switching easier. There are certain parts of it that are even more efficient, such as the torrent information being visible at the bottom of the window in dynamic tabs, rather than having to open a new tab for every torrent you have running. The statistics automatically show up when you open or select a running torrent.

I'm definitely sticking with [Deluge](http://deluge-torrent.org/) as my client. It's fast, reliable, and easy on the system. Finally, I can download the new Ubuntu - because I would never use it to download music :-p
