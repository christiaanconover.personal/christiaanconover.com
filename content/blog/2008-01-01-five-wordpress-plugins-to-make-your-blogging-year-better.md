---
title: Five WordPress Plugins to Make Your Blogging Year Better
date: 2008-01-01T14:13:00.000Z
slug: five-wordpress-plugins-to-make-your-blogging-year-better

tags:
  - google
  - lightbox
  - wordpress plugins
  - analytics
  - wordpress
---

As an avid WordPress user, I have come to rely heavily on some of my plugins. Occasionally I'll browse the WordPress.org plugins section looking for cool ones to try, but usually I stumble across them trying to solve a problem or make my life easier. I've come up with a list of the ones that I find the most useful, or that are cool and worth having anyway.

[**Google Analytics**](http://xavisys.com/wordpress-google-analytics-plugin/): This plugin is crucial to me, perhaps above any of the others I use. I love Google Analytics - it's so easy to use, and it gives you so much information. However, I sometimes get a little frustrated with it in conjunction with WordPress. When I decide I want to switch themes, I have to go through and manually paste the tracking code into each appropriate theme file, which can be frustrating, especially when I realize days later that I missed one. Also, since I'm a frequent visitor to my own blog to make a new post or whatever other reason, that can skew the accuracy of the data being presented. I've used the filter feature in Analytics before, but I'm not always at the same location when I access my blog, so for me it's not that effective. The Google Analytics plugin solves both these problems. It automatically inserts the tracking code you provide into all the pages, making sure that no page view is overlooked - except, of course, for the ones you tell it to ignore. It allows you to choose what user levels not to gather tracking data from, so you won't see all the visits you (or other authors/admins) make to the site, resulting in a more accurate reflection of the visitors to your blog. This is a must-have for any serious WordPress user.

**WordPress.com Stats**: This one complements the Google Analytics plugins, and between the two you get a wicked good picture of who's coming to your blog and what's bringing them there. WordPress.com Stats tracks information specific to bloggers, and displays the stats in real time. That way you can start seeing immediately once you've made a post how long it takes for people to discover it, as well as how they're discovering it. It tracks the number of visitors, search queries that led them there, most popular posts, as well as other information. Another must-have for any real blogger.

[**Advanced TinyMCE Editor**](http://www.mkbergman.com/?page_id=383): Let's face it, the editor that comes with WordPress is a little sparse. beyond very basic text decorations and inserting links and pictures, there's not a whole lot you can do with it. That's a little frustrating for software designed for a type of web site that's all about creating content. Advanced TinyMCE Editor is the answer to every dissatisfied WordPress blogger's prayers. It provides you with a full-featured editor like you'd expect to find in an office suite or a word processing web application. More advanced text decoration tools, table modification tools, spellcheck, and a lot more that you'll have to try.

**CForms II**: A contact form is a great way to let readers feel more connected to you, and to get direct feedback about your blog beyond the comments left on individual posts. I've tried a number of different form plugins, but none of them have come close to cforms II. Unlike any others I've found, this one lets you create your own forms using a graphical system, and even allows you to use Captcha image verification, auto-clearing and requiring fields, all with the click of a mouse - no code knowledge needed! You can have multiple forms if you'd like to allow readers to contact different people, or want different information depending on the page they visit. The features and options available with this plugin are immense. When you insert the form into a page, it does include a small credit link at the bottom of a form. With most of the plugins I've found, that turned me off of them. However, this one's so flexible and powerful that I was willing (even happy) to help the creator out. There's no other contact form plugin worth having, so definitely check this out.

**[Lightbox](http://www.stimuli.ca/lightbox/) with [Add Lightbox](http://mdkart.fr/blog/plugin-add-lightbox-pour-wordpress/)**: Lightbox is an awesome plugin that displays images linked on your blog in a cool flash viewer on top of the page. Add Lightbox automatically inserts the `rel` attribute into the image link so you won't have to manually set up using Lightbox on every image. When combined, it allows your readers to look at pictures on your blog much more easily and quickly, while still making it easy for you to take advantage of this.

These plugins are a great start to making blogging easier and better in 2008\. Slap these in, stir up some content, and you're good to go! Happy blogging in 2008!
