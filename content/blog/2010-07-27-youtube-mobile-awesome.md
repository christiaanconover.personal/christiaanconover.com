---
title: Why the New YouTube Mobile Site is Better Than the Mobile Apps
date: 2010-07-27T14:13:38.000Z
slug: youtube-mobile-awesome

tags:
  - android
  - iphone
  - youtube
---

A couple of weeks ago [YouTube](http://youtube.com/) [released their new mobile site](http://youtube-global.blogspot.com/2010/07/youtube-mobile-gets-kick-start.html), which added some really cool features. The most notable: watch videos directly from the mobile site in a device-native format, without the need for a dedicated app. Aside from this being really cool from a technical standpoint - as an indicator of the true potential for shifting from platform-specific apps to web apps that can run on any device - it's important from a philosophical & content-centric standpoint. I've been using the mobile site instead of the Android app on my phone for the past 10 days or so, and I've come to the conclusion that it really is a better user experience.

Before we go any further, let's take a look at the official demo video that YouTube put out with the release of the new site:

{{< youtube GGT8ZCTBoBA >}}

There are some important things to note in this video. First off, the native app feel of the interface: the web site & the videos themselves look & feel like they were part of an app written specifically for & installed on each device being shown. The video seamlessly transitions when the phone is changed from portrait to landscape orientation. The site is smooth & behaves like a native app. Everything about it is very polished, and the best part: **it looks & behaves exactly the same across multiple devices**. Android, iPhone - it doesn't matter what you're using, you can have the same experience as everyone else.

There's an even deeper & more important element: access to YouTube's content can't be controlled by a platform developer. YouTube is the biggest video sharing site on the planet - 20 hours of video are uploaded every minute - and is an integral part of the modern media & content consumption experience. Why allow various device manufacturers, OS developers, etc. to decide on a whim they no longer support YouTube by banning its app from their platform? By making an awesome mobile web app, control is put back in the hands of YouTube, and most importantly the users.

As I mentioned above, there's an element of cool to any web geek out there who's excited by the prospect of full-featured mobile web apps. This is one of the first major examples of a web interface being as good as, or better than, native software on a mobile device. This is true proof of concept in a significant way, not only in its reach of user base, but in the fact that it's tackling something that can be very tricky: cross-platform video compatibility. YouTube and Google has made a strong move in the progress toward HTML5 and rich web apps, in some ways technically but even more importantly - symbolically.

I'll get down off my soapbox & back to the facts: YouTube has done an excellent job with the new mobile web site. It's so good that I've replaced the YouTube shortcut on my phone's home screen from the one for the native app to a bookmark directly to YouTube mobile.

I can't wait to see what they do next.