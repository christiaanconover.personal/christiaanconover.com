---
title: "What I'm Reading"
description: 'The tumultuous beginnings of Twitter, thoughts on the tech behind HealthCare.gov, and more.'
date: 2013-10-25T15:44:27.000Z
slug: what-im-reading-22
image:
  src: /files/blog/what-im-reading-22/market-street.jpg
  alt: Street car on Market Street

tags:
  - healthcare.gov
  - twitter
  - "what i'm reading"
  - wordpress
---

[**All is Fair in Love and Twitter**](http://www.nytimes.com/2013/10/13/magazine/all-is-fair-in-love-and-twitter.html?pagewanted=1&_r=0&pagewanted=all)<br>
The origins of Twitter are steeped in drama, betrayal and intrigue.

[**Why the Government Never Gets Tech Right**](http://www.nytimes.com/2013/10/25/opinion/getting-to-the-bottom-of-healthcaregovs-flop.html?_r=0)<br>
HealthCare.gov is just one of a number of major government tech projects that have failed. Here's why, and how it can be fixed.

[**Why didn't the White House use WordPress?**](http://www.politico.com/blogs/media/2013/10/healthcaregov-why-didnt-the-white-house-use-wordpress-175764.html)<br>
Five of the 14 states running their own exchanges use WordPress, with much greater success than HealthCare.gov. Why didn't the federal government opt for an existing open source platform?

[**Space Invaders: Why you should never, ever use two spaces after a period**](http://www.slate.com/articles/technology/technology/2011/01/space_invaders.html)<br>
Most of us were taught at some point that two spaces between sentences is correct. We've been taught wrong.
