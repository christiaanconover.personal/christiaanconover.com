---
title: Alex Albrecht has a BMW M3
date: 2010-03-31T17:11:43.000Z
slug: alex-albrecht-has-a-bmw-m3

tags:
  - '#BMWBunch'
  - bmw
  - bmw m3
---

[Alex Albrecht](https://en.wikipedia.org/wiki/Alex_Albrecht), cohost of the show [Diggnation](http://bit.ly/dDafs6) and a few other shows on [Revision3](http://bit.ly/c0sNZs), also happens to be a BMW guy. For a while he's been leasing a 335i and has mentioned it a few times on Diggnation. Recently though, his lease came up and he opted to make the jump into ///M - and leased an M3 convertible! He talked about it on the [most recent episode of Diggnation](http://bit.ly/b9xa8i) & even though he's limited to 350bhp since he's still in the break-in, he still says it's absolutely "ah-mazing." Glad to hear he realizes what a sick car he drives! Alex, welcome to the finest community of automobile owners on Earth. May I suggest [joining the BMW Car Club of America](http://bit.ly/9rjzaQ), and hopping in on [#BMWBunch](http://bit.ly/cyklUq) from time to time?

In other news, Diggnation cohost and founder of Digg [Kevin Rose](http://bit.ly/aelolU) wants to buy an [Audi RS5](http://bit.ly/aa5VYJ) when they come to the U.S. Pretty sweet car, but I think we need to push for Diggnation to do a series of head-to-head tests when that happens. Not just drag racing, but corners too.

Anyway, that's the celebrity news I have, congrats Alex!

**Update:** it seems that as I started writing this, Alex went in to get his M3's 1200 mile break-in service done, and the horsepower limiter removed. We want to hear your thoughts on all 414bhp, Alex!

{{< tweet 11376386378 >}}

[![Alex Albrecht and his M3](/files/blog/alex-albrecht-has-a-bmw-m3/albrecht-m3.jpg)](http://bit.ly/alEakc)<br>
*Photo courtesy of [Alex Albrecht](http://bit.ly/9pUwdu) via [YFrog](http://bit.ly/alEakc)*