---
title: "Today's Net Neutrality Decision Screws Us All"
description: 'Net neutrality is critical to free speech, and a strong and growing economy, and it just got struck down. We have to fight to get it back.'
date: 2014-01-14T20:55:44.000Z
slug: todays-net-neutrality-decision-screws-us-all
image:
  src: /files/blog/todays-net-neutrality-decision-screws-us-all/net-neutrality-dead.png
  alt: Net neutrality is dead

tags:
  - fcc
  - net neutrality
  - verizon
---

A federal appeals court today [struck down some key provisions](http://www.theverge.com/2014/1/14/5307650/federal-court-strikes-down-net-neutrality-rules) of the FCC [Open Internet order](https://en.wikipedia.org/wiki/FCC_Open_Internet_Order_2010). The court determined that, because ISPs do not have "common carrier" classification the way telephone providers do, the FCC does not have the authority to force ISPs to treat all traffic equally. This is a massive blow to the very foundation of what makes the Internet work the way it does, and has potentially devastating consequences.

As it stands now (or at least until today's ruling), your ISP cannot favor traffic from certain sites or services over others. Whether you choose to stream from Netflix, or your provider's video-on-demand service, your ISP has to carry that traffic with the same priority. After today, your service provider (such as Verizon, who brought the suit that resulted in today's ruling) can now charge extra for anything it pleases. Oh, but they have to tell you they're doing so - that's the only provision that survived.

"Yeah yeah, this is a geek problem. It doesn't affect me." Think again, my friend. Let's run through a scenario that might bring this one home for you.

> **You**: "Man, YouTube and Netflix are loading really slowly today. Let me call my ISP to find out if there's a problem."

> **ISP**: "Thank you for calling [your ISP here], how may I help you?

> **You**: "I'm seeing really slow speeds trying to get to YouTube, is there a service problem in my area?"

> **ISP**: "Oh no, there's no issue. We've recently 'upgraded' our plans to serve you better and you don't have the Video Plus package. At the moment you only have unlimited access to [your ISP here]'s Stream Awesome video-on-demand platform, which we include for being such a valued customer! For an additional $8.99 a month you can watch videos from all those other, lesser sites too. What a great deal!"

> **You**: "Wait a minute, I've never had to pay for this before. Why do I need to now?"

> **ISP**: "Well, we get to nickel and dime you to death, and nobody can stop us!"

You can swap out YouTube and Netflix for Facebook and Twitter, or Gmail instead of your ISP's email service, or Google search instead of whatever search system your ISP prefers, or Amazon instead of QVC. You no longer get the Internet, you get your ISP's version of the Internet as it suits them best, and to hell with what customers want because they don't have a choice.

In the above scenario you can get to those other sites, but they'll either be extremely slow or cost you more money. However, there's nothing to say your service provider won't block them entirely because they conflict with services offered by your ISP. Take [Aereo](http://aereo.com) for example, a service that lets you stream broadcast television over the Internet. It directly competes with the cable TV packages your ISP likely offers, so they might want to block that entirely and force you to pay for cable TV.

**"Gee, it'd be a real shame if something were to happen to that nice little company of yours. We can help with that."** Alternatively, your ISP could offer those other companies the option to take on the cost burden by paying a per-customer fee. The end user wouldn't see the fee on their bill, but they'd likely see its impact in the cost of those services. Big companies like Google, Amazon, Netflix, et al might have the resources to take on this burden. However, this pretty much makes any startup that can't pony up the protection racket money dead in the water. Innovation and growth will falter, and we'll eventually end up with an Internet that looks pretty much exactly like the cable TV market.

Do you run a site that ranks service providers, or allows customer reviews of how good or bad their ISPs are? Prepare to be blacklisted, and fall off the face of the Internet. There's no way that service providers are going to let their customers write and see bad reviews about their products.

There is a glimmer of good news: the FCC can still fix this. They can rewrite the rules, and assert their authority over the Internet with a solid legal footing that will stand up against lawsuits, and prevent ISPs from censoring the Internet. FreePress has [a petition to push the FCC to establish better net neutrality rules](http://act.freepress.net/sign/internet_FCC_court_decision2), which you should absolutely sign. Reach out to your representatives and senators, asking them to require the FCC to establish net neutrality.

We have to fix this. If net neutrality isn't re-established and ISPs get to do whatever they want with the Internet, everybody loses.
