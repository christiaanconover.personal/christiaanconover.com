---
title: "What I'm Reading"
date: 2013-07-10T15:13:42.000Z
slug: what-im-reading-10
image:
  src: /files/blog/what-im-reading-10/ocean-sunset.jpg
  alt: Sunset over the ocean

tags:
  - smartwatch
  - "what i'm reading"
---

**[Summer May Be Best Time to Make Babies](http://www.wired.com/wiredscience/2013/07/summer-babies/?cid=co9638454)**<br>
Not quite what you think it is. But also, exactly what you think it is.

**[From Subject Line To Signature: How To Do Work E-Mail Right](http://www.forbes.com/sites/jacquelynsmith/2013/07/08/subject-line-to-signature-how-to-do-work-e-mail-right/)**<br>
Most work emails suck. Do your part to make them suck less.

**[Opinion: Military Pay and Benefits Unsustainable](http://news.usni.org/2013/07/10/opinion-military-pay-and-benefits-unsustainable)**<br>
We need to start having serious conversation, unfettered by hyperbole, about the massive and growing costs of servicemember pay & retirement benefits.

**[Hands Off My Wrists! Why I Don't Want To Wear My Computers](http://readwrite.com/2013/07/10/no-smart-watches-wearable-computing?awesm=readwr.it_fFK#awesm=~obcqSa6Biwvnay)**<br>
I disagree about wanting a smartwatch, but he raises some interesting points.
