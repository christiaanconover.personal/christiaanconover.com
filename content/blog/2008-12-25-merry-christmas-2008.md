---
title: 'Merry Christmas!'
date: 2008-12-25T23:16:50.000Z
slug: merry-christmas-2008

tags:
  - christmas
---

Merry Christmas to everyone!

As we speak, Sea Term 2009 officially begins in 1 week, 3 days and change. I've added a countdown timer on the sidebar right below my picture which currently displays the up-to-the-second count until the official start of Sea Term.

I'll be continuing my pre-Sea Term info posts tomorrow.
