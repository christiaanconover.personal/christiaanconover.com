---
title: Pebble Smartwatch Unboxing and First Impressions
date: 2013-03-25T22:35:44.000Z
slug: pebble-unboxing
image:
  src: /files/blog/pebble-unboxing/in-the-box.jpg
  alt: Pebble in the box
  caption: "Sparsity really is the name of the game with Pebble's packaging."

tags:
  - gadgets
  - internet of things
  - kickstarter
  - pebble
  - smartwatch
  - unboxing
---

It's here! After months of [waiting](http://www.kickstarter.com/projects/597507018/pebble-e-paper-watch-for-iphone-and-android/posts/302146) and [numerous delays](http://www.kickstarter.com/projects/597507018/pebble-e-paper-watch-for-iphone-and-android/posts/273665), my [Pebble smartwatch](http://getpebble.com/) has finally arrived. Even though I've only had it for about an hour, I can already tell the wait was worth it. It feels good to wear, it looks good, and the interface is pleasantly simple. This post isn't a review though, so let's look at what we're here for: the unboxing.

Pebble's product box is also its shipping box, reducing the amount of packaging required. It's plain, two-tone cardboard with some basic information about Pebble such as phone compatibility.

![Pebble's box, front and back](/files/blog/pebble-unboxing/box.png)<br>
Pebble's box, front and back

The simplicity continues on the inside. The box contains the Pebble watch, the USB charging cable - and that's it. There's a small graphic printed inside the lid telling you to go to the web site for setup instructions.

Pebble is so minimalist, in fact, that it doesn't even come with a power adapter. The instructions say to just plug the USB cable into your computer or a power adapter you already own. Considering how many USB wall warts most of us likely own at this point, it's actually a brilliant move on their part.

![Because I backed Pebble on Kickstarter, my watch has "Kickstarter Edition" written on the back.](/files/blog/pebble-unboxing/back.jpg)<br>
Because I backed Pebble on Kickstarter, my watch has "Kickstarter Edition" written on the back.

In order to do anything with the Pebble, you need to have an iPhone or Android phone. Naturally, the first step is to download the Pebble app. Once you've done this, the app walks you through pairing your Pebble with your phone. Pebble uses Bluetooth, so it's the same process as connecting a headset.

![Pebble's app home screen, and some of the setup instructions from the web site.](/files/blog/pebble-unboxing/setup.png)<br>
Pebble's app home screen, and some of the setup instructions from the web site.

When you first power Pebble on, it also tells you to go to the web site for setup instructions. The first thing the instructions tell you is how to charge the Pebble, and like all electronics it's a good idea to fully charge it before you start using it (difficult as it may be to resist the temptation to put it right on to your wrist!).

![Pebble's screen the first time you power it up.](/files/blog/pebble-unboxing/first-run.jpg)<br>
Pebble's screen the first time you power it up.

Once you've paired Pebble with your phone, there's really nothing else you have to do. It may prompt you to update Pebble's firmware, which is a one-click process. Pebble comes with a couple of watch faces pre-installed, so you don't have to install anything to start using the watch. Installing new watch apps, which includes watch faces, is done through the app and is as simple as tapping on the one you want. You can also configure notifications through the app, so that Pebble will alert you to incoming phone calls, texts, emails, calendar reminders, etc. and even allows you to control your phone's music app.

![Pebble's stock watch faces.Left to right: Text Watch, Classic Analog, Fuzzy Time](/files/blog/pebble-unboxing/faces.png)<br>
Pebble's stock watch faces. Left to right: Text Watch, Classic Analog, Fuzzy Time

As I mentioned at the top, I've only had my Pebble for about an hour so I'm not ready to do any sort of a review - that'll come later after I've used it in the real world for a while.
