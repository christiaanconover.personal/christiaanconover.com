---
title: 'New Series: Starting At MMA'
date: 2008-03-06T17:24:01.000Z
slug: new-series-starting-at-mma

tags:
  - mass maritime
  - starting at mma
---

I received an e-mail from the parent of a student who will be a freshman at MMA in the fall. She suggested that I write about important aspects of starting out as a freshman at MMA. Over the next week or two, I plan to cover a variety of topics pertaining to Orientation and first semester freshman year. If you have anything specific about the beginning of freshman year that you'd like me to cover, please feel free to post a comment and I'll try to cover it.
