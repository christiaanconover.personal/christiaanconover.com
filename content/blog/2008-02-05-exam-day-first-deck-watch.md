---
title: 'Exam Day, First Deck Watch'
date: 2008-02-05T13:10:06.000Z
slug: exam-day-first-deck-watch

tags:
  - mass maritime
  - sea term
  - sea term 2008
---

Yesterday was exam day for the first half of Sea Term. Everybody by this point has had a rotation of license training (either deck or engine) and a rotation of non-license training (either MSEP or Emergency Management & Business). There is a final exam for each major's training, so testing is done halfway through cruise, and again at the end of cruise for the other half of training received. Today I had the Engine exam and the MSEP exam. At the moment it's about 0400 local time (0500 EST) and I just got off my first at-sea deck watch, where I started out as the stern lookout. On deck watch, everyone starts at a certain position, and is rotated every hour. From the stern I went to the bridge, where I spent some time learning about the various systems & operations there, and had an opportunity to be the helmsman! Steering the ship is a unique experience, because it doesn't respond like any other vehicle I've ever driven. It took about 20 minutes for me to start getting a sense of how the rudder responded to the wheel. I'm confident that by the end of deck watch in a few days I'll be a pretty decent helmsman. I'm looking forward to being on the bridge during the day so I can see more of what's going on around us from that vantage point. Right now, however, I'm headed to bed.
