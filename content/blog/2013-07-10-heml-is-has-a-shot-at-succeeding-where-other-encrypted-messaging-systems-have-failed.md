---
title: Heml.is Has a Shot at Succeeding Where Other Encrypted Messaging Systems Have Failed
date: 2013-07-10T22:06:49.000Z
slug: heml-is-has-a-shot-at-succeeding-where-other-encrypted-messaging-systems-have-failed
image:
  src: /files/blog/heml-is-has-a-shot-at-succeeding-where-other-encrypted-messaging-systems-have-failed/hemlis-logo.png
  alt: Heml.is logo

tags:
  - edward snowden
  - encrypted messaging
  - encryption
  - nsa
  - prism
---

[Heml.is](https://heml.is/) (pronounced without the dot) was announced yesterday as a new messaging platform built from the ground up to be secure and private. Their goal is to develop an app for iOS and Android that uses proven open standards and high-grade private key encryption to keep your conversations totally encrypted and away from prying eyes, while being user-friendly and attractive. They're obviously striking a nerve with users, because [in ~~less than~~ 36 hours they've managed to raise ~~nearly 90%~~ their $100,000 funding goal](http://hemlismessenger.wordpress.com/2013/07/11/funded-100-in-36-hours/).

They've got a very lofty goal and one that, having watched many a secure messaging tool come and go, I'm a bit skeptical they'll achieve. Yet I'm hopeful because they seem to be doing all the right things to get mainstream appeal for their system.

**Their timing is impeccable**. With all the [news swirling around NSA and PRISM](http://en.wikipedia.org/wiki/PRISM_(surveillance_program)), plenty of people are looking for ways to keep better control over who really sees what they're saying. It's not to say everyone is talking about illegal activity, but all the same we'd rather have a little privacy. Heml.is is showing up right when the whole situation is exploding and offering a viable option.

**The mockups of their app look great**. I've used other encrypted messaging apps. They all suck. There's no way I'd be able to convince the rest of my family to use one of them. Not only are their designs ugly and awkward to navigate, but setting up all the crypto and servers and key exchanging is a major pain that they won't be bothered with. I'll quickly be met with the same exasperation I encounter any time I try to "fix" the security of their WiFi or "fix" their password usage. Heml.is looks (from the demo video) to be very attractive and easy to use, mimicking the design of your typical text or instant messaging app that people are used to. Because they're building it with their own infrastructure rather than a federated, self-hosted approach typical to most other encrypted messaging, they should be able to handle the exchange of keys with ease, and without annoying the user. My family might actually use this thing.

**They have a business model**. They're charging what appears to be a very modest fee for more advanced features like picture messaging, and text messages will always be free. That means they'll have a vested interest in continuing to develop it, and making sure it works well enough to get broad appeal. It's not another open source project hoping to offer the perfect solution to everyone's secure messaging problems to be used however anyone wants, but ultimately only getting traction from people with enough technical skill to operate the cumbersome software. They're building on open source tools and want to open source their own code, but they're a company developing a service that needs money to operate, and needs users to pay the bills. They need to make sure the revenue keeps coming in by making a working, appealing product.

I'd really like to see them succeed because having a reliable, user-friendly way to send and receive encrypted messages is something we really need, even if we don't all realize it yet.

*If you want to help fund them, you can do so [at their web site](https://heml.is).*