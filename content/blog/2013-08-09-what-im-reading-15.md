---
title: "What I'm Reading"
date: 2013-08-09T23:40:04.000Z
slug: what-im-reading-15
image:
  src: /files/blog/what-im-reading-15/trey-ratcliff-batman-running.jpg
  alt: '"Batman Running" by Trey Ratcliff'

tags:
  - 3d printing
  - anil dash
  - apple
  - automattic
  - iwatch
  - marijuana
  - medicine
  - trey ratcliff
  - "what i'm reading"
  - wordpress
---

[**Great Place to Work: At Automattic Employees All Work From Home And Travel To Exotic Locations**](http://www.businessinsider.com/automattics-awesome-remote-work-culture-2013-8)<br>
The company behind WordPress has a culture of decentralized employment, and it's working well. Take note, other companies everywhere.

[**Why I changed my mind on weed**](http://edition.cnn.com/2013/08/08/health/gupta-changed-mind-marijuana/index.html)<br>
Dr. Sanjay Gupta of CNN is now in favor of medical marijuana. Bonus: his explanation is based in science, not rhetoric.

[**A Brief History of Apple's iWatch**](http://dashes.com/anil/2013/08/a-brief-history-of-apples-iwatch.html)<br>
Yes, iWatch isn't even real yet. That doesn't mean we don't already know exactly how things will shake out. Warning: this is serious tech inside baseball. You must be --this-- geeky to appreciate it.

[**How 3-D Printing Body Parts Will Revolutionize Medicine**](http://www.popsci.com/science/article/2013-07/how-3-d-printing-body-parts-will-revolutionize-medicine)<br>
If you need any more proof than this that we live in the future and it's amazing, you're wrong.
