---
title: Twelve Years in Annapolis
date: 2013-04-02T15:10:57.000Z
slug: 12-years-in-annapolis
image:
  src: /files/blog/12-years-in-annapolis/md-state-house-snow.jpg
  alt: Maryland State House

tags:
  - annapolis
  - coast guard reserve
  - mass maritime
---

Today marks the twelfth anniversary since my family and I moved to Annapolis. I was 12 years old when we moved here, which means that I've now lived here for half my life. Being a military brat it's still hard to imagine that I've really lived in one place for that long.

Staying in Annapolis was not originally part of my plan. When I was in high school my goal was to attend the Coast Guard Academy, commission as a Coast Guard officer and live anywhere & everywhere. That didn't come to pass and [I enlisted in the Coast Guard Reserve](/blog/uscgr-enlisting/), which was a great decision and allowed me to move back home.

Despite moving every few years for the first half of my life and believing I'd have trouble staying put for much longer, I've happily grown roots here. I [went away to college](/tags/mass-maritime/) for a few years but came back. Last summer I moved into my first apartment right in downtown, and have no plans to leave the area.

If you'd have told me even four years ago that I'd be establishing my adult life in Annapolis and planning to be here for the foreseeable future, I'd have laughed out loud. Yet that's exactly what's happened, and I couldn't be happier.
