---
title: Some Entertainment on Watch
date: 2008-04-04T11:50:19.000Z
slug: some-entertainment-on-watch

tags:
  - cic
  - mass maritime
  - watchstanding
---

Wednesday night/Thursday morning I had watch 2330-0330 in the Cadet Information Center. As a freshman, I have to do rounds in either the even or odd numbered companies, and the other freshmen does the other ones. We are listed as messengers, so we also are sent to run errands if need be. Usually that shift and watch post is pretty boring, and rarely does anything happen whatsoever.

This time, however, we did get a little entertainment. Around 0230 we were sitting in the Cadet Information Center, watching the seconds go by on the clock (if there had been paint drying we definitely would have watched that instead to kill the boredom), when two girls who don't go to school here came around the corner, giggling and arguing about what to do, and how they were going to find who they were looking for. They stood in the hallway caddy corner to CIC where we could see them in plain sight, which they obviously didn't realize. As it turned out, they didn't even realize we were there until the Second Class on watch walked over to them to find out who they were and why there were here at 2:30 in the morning. As soon as they saw him, they freaked out a little bit and started "whispering" to each other about the man in uniform approaching them. Apparently they weren't aware that we all wear uniforms, and were under the impression that he was a cop. After a few minutes of trying to get any usable information from them, we finally determined that they were looking for two cadets, but didn't know where they lived and had been wandering around for quite a while, knocking at random doors to try to find them (bear in mind what time it is). We told them they should probably just leave before somebody annoyed at being woken up called Campus Security, which they agreed was probably a good idea. They walked off the same way they came.

About 20 minutes later I went to do my rounds, and I found the same two girls still wandering around! As soon as they saw me, they took off in the other direction, ran outside and got in their car and left. I think they were convinced that everyone here was a cop, and that the heat was on to find them. It was definitely the highlight of watch.

**Side Note:** I will be at Welcome Aboard tomorrow, so if you're going to be there, look for me at the Sea Term station.
