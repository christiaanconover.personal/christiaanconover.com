---
title: "MMA's Chain of Command: From the President to the 4/C Cadet, and Everyone In Between"
date: 2008-05-01T12:12:15.000Z
slug: mmas-chain-of-command-from-the-president-to-the-4c-cadet-and-everyone-in-between

tags:
  - cadet officers
  - chain of command
  - comcad
  - company officers
  - mass maritime
  - regimental staff
---

Mass Maritime has a multi-layer chain of command, due to the co-existence of cadet and staff officers and leaders. I've created an overview of the chain of command below.

**Staff Officers**<br>
President of the Academy<br>
Vice Presidents<br>
Commandant of Cadets<br>
Vice Commandant of Cadets<br>
Company Officers

**Cadet Officers**<br>
Regimental Commander<br>
Regimental Executive Officer<br>
Regimental Staff<br>
Company Commanders<br>
Company Executive Officers<br>
Company Staff<br>
Platoon Leaders<br>
Squad Leaders<br>
Petty Officers<br>
Fourth Class Cadets

# Staff Officers

**President of the Academy ([Adm. Richard Gurnon](http://www.maritime.edu/index.cfm?pg=directory&empid=95)):** Admiral Gurnon is the very top of the chain of command. He is in charge of all aspects of the Academy - academics, the regiment, campus business, etc.

**Vice Presidents (Capt. Allen Hansen & Capt. Bradley Lima):** We have two Vice Presidents. Capt. Hansen is the Vice President of Student Services, and Capt. Lima is the Vice President of Academic Affairs.

**Commandant of Cadets (Capt. Edward Rozak):** The Commandant of Cadets is the head of the regimental aspect of the Academy. His office is in the dorm complex, whereas the officers above all have offices in the Harrington Building. His office is in charge of day-to-day activities within the regiment, as well as the cadet officer program.

**Vice Commandant of Cadets (Cdr. Stephen Kelleher):** The Vice Commandant of Cadets supports the Commandant of Cadets, and assists with the disciplinary process at the Academy.

**Company Officers:** There are six companies, and each company has its own staff officer.

- 1st Company: [Lcdr. Douglas Page](http://www.maritime.edu/index.cfm?pg=directory&empid=73)
- 2nd Company: Lt. Michael Kelley
- 3rd Company: Cdr. Joseph Domingos
- 4th Company: Lt. Marie Huhnke
- 5th Company: Lt. Samuel White
- 6th Company: Lt. Nancielee Holbrook

The company officers oversee the company's cadet officers, and handle discipline issues within the company. They also do 9's and 10's, which is a walk through of the company and random rooms to make sure they are up to standards.

# Cadet Officers

**Regimental Commander:** The Regimental Commander is the cadet in charge of the regiment. He works directly with the Commandant of Cadets to handle day-to-day operations of the regiment. The Regimental Commander has six rank bars on his uniform.

**Regimental Executive Officer:** The Regimental XO is the second in command among cadet officers in the regiment. In addition to supporting the Regimental Commander, he is one of the members of the Honor Board, and sits on the hearing board for Class II offenses. The Regimental XO has five rank bars on his uniform.

**Regimental Staff:** The various other members of the regimental staff all hold equal rank and authority within the regiment. These include the Regimental Operations Officer, the Regimental Adjutant, and the Regimental Training & Retention Officer. All Regimental Staff officers have five rank bars on their uniforms.

**Company Commanders:** Company commanders are the cadets in charge of each company. They work directly with the company officer for their company. They have four rank bars.

**Company Executive Officers:** The Company XO assists the Company Commander, and is also in charge of company discipline. The XO has three rank bars.

**Company Staff:** The staff positions for the company are the same as the regimental staff, just within company instead of the entire regiment. They have three rank bars.

**Platoon Leaders:** Platoon leaders are each in charge of a deck within the company, which puts them in charge of all the cadets of a given class within the company. For example, the 03 deck platoon leader is in charge of all fourth class (freshman) cadets in the company, since the freshmen all live on the 03 deck. Third class live on the 02 deck, and second & first class live on the 01 deck. The company commander and XO live on the 00 deck. Platoon leaders have two rank bars.

**Squad Leaders:** Squad leaders are second class cadets who work under the 03 deck platoon leader. They are in training to try for officer positions when they are first class cadets. They are mainly responsible for assisting with the fourth class cadets, which is why they work for the 03 deck platoon leader. They have no bars, but instead have two chevrons.

**Petty Officers:** Petty officers are assigned in the second semester of each year. These are third class cadets who are in training to become squad leaders. They take over some of the more basic responsibilities of the squad leaders, while the squad leaders are starting to take on the responsibilities of any officer positions they may have achieved.

**Fourth Class Cadets:** Fourth Class have no position or authority. These are freshman cadets, still going through the most basic training process.
