---
title: '2014: The Year of Tangible Accomplishments'
date: 2014-01-01T16:30:39.000Z
slug: 2014-the-year-of-tangible-accomplishments

tags:
  - new year
---

It's a new year! Huzzah! Let's celebrate new beginnings and look forward to the good times ahead!

Yes, facetiousness abounds. In truth though, that's what we're all doing. Rather than fight it and scoff like I have in years past, I'm going to embrace it.

2013 was, [despite my optimistic outlook](/blog/welcome-to-2013/), a fairly lackluster year. I pretty much maintained status quo, and didn't make much forward progress. It was, by all accounts, a placeholder year. It wasn't a bad year per se; I have a number of good memories from the past twelve months. I just don't have anything I can specifically point to as a significant accomplishment.

2014 will be different. In the past 6 weeks or so I've started laying the groundwork for the personal improvements I'm aiming to make.

**Beef up my coding skills**. It's time for some professional development. I released [a couple of WordPress plugins](/code/) at the end of 2013, but those are the beginning. I have big plans to expand and improve my [Author Customization](https://wordpress.org/plugins/author-customization/) plugin. I have a few open source projects on my radar that I'm getting involved with. I'm going to learn a new language or two so I'll be more versatile and capable. 2013 mostly consisted of dabbling here and there with bits of code with no real focus. 2014 will be the year of shipping code and soaking up knowledge.

**Get in shape**. Yes, I know it's cliché to say I'll exercise more. Once again, I've already taken some steps to ensure success on this front. My parents gave me a gym membership for my birthday, so I've gotten into an exercise routine that I've (mostly) stuck to. My diet could use some work, so that's what I'll be tackling in the new year. My benchmark on this front is to lose my beer gut, and be able to comfortably run a 5K. That's not to say I want to run it - quite the opposite, I can't stand running. The Coast Guard makes me run every so often though, so I'd like to not dread having to do it. Anything beyond those metrics is a bonus, and also I don't know (yet) what would make good goals above and beyond the ones I've stated.

So there we have it. My stated objectives for the year. A group of measurable objectives that are attainable and challenging. 2014, let's get to it!
