---
title: "{{ replace .TranslationBaseName "-" " " | title }}"
description: "Brief summary of the post"
slug: "{{ .TranslationBaseName | urlize }}"
date: {{ .Date }}
image:
    src: "image"
    alt: "image info"
tags:
    -
draft: true
---
